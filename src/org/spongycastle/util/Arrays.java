package org.spongycastle.util;

import java.math.BigInteger;

public final class Arrays {
	private Arrays() {
	}

	public static byte[] append(byte[] var0, byte var1) {
		if (var0 == null) {
			return new byte[] { var1 };
		} else {
			int var2 = var0.length;
			byte[] var3 = new byte[var2 + 1];
			System.arraycopy(var0, 0, var3, 0, var2);
			var3[var2] = var1;
			return var3;
		}
	}

	public static int[] append(int[] var0, int var1) {
		if (var0 == null) {
			return new int[] { var1 };
		} else {
			int var2 = var0.length;
			int[] var3 = new int[var2 + 1];
			System.arraycopy(var0, 0, var3, 0, var2);
			var3[var2] = var1;
			return var3;
		}
	}

	public static short[] append(short[] var0, short var1) {
		if (var0 == null) {
			return new short[] { var1 };
		} else {
			int var2 = var0.length;
			short[] var3 = new short[var2 + 1];
			System.arraycopy(var0, 0, var3, 0, var2);
			var3[var2] = var1;
			return var3;
		}
	}

	public static boolean areEqual(byte[] var0, byte[] var1) {
		boolean var2;
		if (var0 == var1) {
			var2 = true;
		} else {
			var2 = false;
			if (var0 != null) {
				var2 = false;
				if (var1 != null) {
					int var3 = var0.length;
					int var4 = var1.length;
					var2 = false;
					if (var3 == var4) {
						int var5 = 0;

						while (true) {
							if (var5 == var0.length) {
								return true;
							}

							byte var6 = var0[var5];
							byte var7 = var1[var5];
							var2 = false;
							if (var6 != var7) {
								break;
							}

							++var5;
						}
					}
				}
			}
		}

		return var2;
	}

	public static boolean areEqual(char[] var0, char[] var1) {
		boolean var2;
		if (var0 == var1) {
			var2 = true;
		} else {
			var2 = false;
			if (var0 != null) {
				var2 = false;
				if (var1 != null) {
					int var3 = var0.length;
					int var4 = var1.length;
					var2 = false;
					if (var3 == var4) {
						int var5 = 0;

						while (true) {
							if (var5 == var0.length) {
								return true;
							}

							char var6 = var0[var5];
							char var7 = var1[var5];
							var2 = false;
							if (var6 != var7) {
								break;
							}

							++var5;
						}
					}
				}
			}
		}

		return var2;
	}

	public static boolean areEqual(int[] var0, int[] var1) {
		boolean var2;
		if (var0 == var1) {
			var2 = true;
		} else {
			var2 = false;
			if (var0 != null) {
				var2 = false;
				if (var1 != null) {
					int var3 = var0.length;
					int var4 = var1.length;
					var2 = false;
					if (var3 == var4) {
						int var5 = 0;

						while (true) {
							if (var5 == var0.length) {
								return true;
							}

							int var6 = var0[var5];
							int var7 = var1[var5];
							var2 = false;
							if (var6 != var7) {
								break;
							}

							++var5;
						}
					}
				}
			}
		}

		return var2;
	}

	public static boolean areEqual(long[] var0, long[] var1) {
		boolean var2;
		if (var0 == var1) {
			var2 = true;
		} else {
			var2 = false;
			if (var0 != null) {
				var2 = false;
				if (var1 != null) {
					int var3 = var0.length;
					int var4 = var1.length;
					var2 = false;
					if (var3 == var4) {
						int var5 = 0;

						while (true) {
							if (var5 == var0.length) {
								return true;
							}

							long var7;
							int var6 = (var7 = var0[var5] - var1[var5]) == 0L ? 0 : (var7 < 0L ? -1 : 1);
							var2 = false;
							if (var6 != 0) {
								break;
							}

							++var5;
						}
					}
				}
			}
		}

		return var2;
	}

	public static boolean areEqual(Object[] var0, Object[] var1) {
		boolean var2;
		if (var0 == var1) {
			var2 = true;
		} else {
			var2 = false;
			if (var0 != null) {
				var2 = false;
				if (var1 != null) {
					int var3 = var0.length;
					int var4 = var1.length;
					var2 = false;
					if (var3 == var4) {
						int var5 = 0;

						while (true) {
							if (var5 == var0.length) {
								return true;
							}

							Object var6 = var0[var5];
							Object var7 = var1[var5];
							if (var6 == null) {
								var2 = false;
								if (var7 != null) {
									break;
								}
							} else if (!var6.equals(var7)) {
								return false;
							}

							++var5;
						}
					}
				}
			}
		}

		return var2;
	}

	public static boolean areEqual(boolean[] var0, boolean[] var1) {
		boolean var2;
		if (var0 == var1) {
			var2 = true;
		} else {
			var2 = false;
			if (var0 != null) {
				var2 = false;
				if (var1 != null) {
					int var3 = var0.length;
					int var4 = var1.length;
					var2 = false;
					if (var3 == var4) {
						int var5 = 0;

						while (true) {
							if (var5 == var0.length) {
								return true;
							}

							boolean var6 = var0[var5];
							boolean var7 = var1[var5];
							var2 = false;
							if (var6 != var7) {
								break;
							}

							++var5;
						}
					}
				}
			}
		}

		return var2;
	}

	public static byte[] clone(byte[] var0) {
		if (var0 == null) {
			return null;
		} else {
			byte[] var1 = new byte[var0.length];
			System.arraycopy(var0, 0, var1, 0, var0.length);
			return var1;
		}
	}

	public static byte[] clone(byte[] var0, byte[] var1) {
		if (var0 == null) {
			return null;
		} else if (var1 != null && var1.length == var0.length) {
			System.arraycopy(var0, 0, var1, 0, var1.length);
			return var1;
		} else {
			return clone(var0);
		}
	}

	public static char[] clone(char[] var0) {
		if (var0 == null) {
			return null;
		} else {
			char[] var1 = new char[var0.length];
			System.arraycopy(var0, 0, var1, 0, var0.length);
			return var1;
		}
	}

	public static int[] clone(int[] var0) {
		if (var0 == null) {
			return null;
		} else {
			int[] var1 = new int[var0.length];
			System.arraycopy(var0, 0, var1, 0, var0.length);
			return var1;
		}
	}

	public static long[] clone(long[] var0) {
		if (var0 == null) {
			return null;
		} else {
			long[] var1 = new long[var0.length];
			System.arraycopy(var0, 0, var1, 0, var0.length);
			return var1;
		}
	}

	public static long[] clone(long[] var0, long[] var1) {
		if (var0 == null) {
			return null;
		} else if (var1 != null && var1.length == var0.length) {
			System.arraycopy(var0, 0, var1, 0, var1.length);
			return var1;
		} else {
			return clone(var0);
		}
	}

	public static BigInteger[] clone(BigInteger[] var0) {
		if (var0 == null) {
			return null;
		} else {
			BigInteger[] var1 = new BigInteger[var0.length];
			System.arraycopy(var0, 0, var1, 0, var0.length);
			return var1;
		}
	}

	public static short[] clone(short[] var0) {
		if (var0 == null) {
			return null;
		} else {
			short[] var1 = new short[var0.length];
			System.arraycopy(var0, 0, var1, 0, var0.length);
			return var1;
		}
	}

	public static byte[][] clone(byte[][] var0) {
		if (var0 == null) {
			return null;
		} else {
			byte[][] var1 = new byte[var0.length][];

			for (int var2 = 0; var2 != var1.length; ++var2) {
				var1[var2] = clone(var0[var2]);
			}

			return var1;
		}
	}

	public static byte[][][] clone(byte[][][] var0) {
		if (var0 == null) {
			return null;
		} else {
			byte[][][] var1 = new byte[var0.length][][];

			for (int var2 = 0; var2 != var1.length; ++var2) {
				var1[var2] = clone(var0[var2]);
			}

			return var1;
		}
	}

	public static byte[] concatenate(byte[] var0, byte[] var1) {
		if (var0 != null && var1 != null) {
			byte[] var2 = new byte[var0.length + var1.length];
			System.arraycopy(var0, 0, var2, 0, var0.length);
			System.arraycopy(var1, 0, var2, var0.length, var1.length);
			return var2;
		} else {
			return var1 != null ? clone(var1) : clone(var0);
		}
	}

	public static byte[] concatenate(byte[] var0, byte[] var1, byte[] var2) {
		if (var0 != null && var1 != null && var2 != null) {
			byte[] var3 = new byte[var0.length + var1.length + var2.length];
			System.arraycopy(var0, 0, var3, 0, var0.length);
			System.arraycopy(var1, 0, var3, var0.length, var1.length);
			System.arraycopy(var2, 0, var3, var0.length + var1.length, var2.length);
			return var3;
		} else if (var0 == null) {
			return concatenate(var1, var2);
		} else {
			return var1 == null ? concatenate(var0, var2) : concatenate(var0, var1);
		}
	}

	public static byte[] concatenate(byte[] var0, byte[] var1, byte[] var2, byte[] var3) {
		if (var0 != null && var1 != null && var2 != null && var3 != null) {
			byte[] var4 = new byte[var0.length + var1.length + var2.length + var3.length];
			System.arraycopy(var0, 0, var4, 0, var0.length);
			System.arraycopy(var1, 0, var4, var0.length, var1.length);
			System.arraycopy(var2, 0, var4, var0.length + var1.length, var2.length);
			System.arraycopy(var3, 0, var4, var0.length + var1.length + var2.length, var3.length);
			return var4;
		} else if (var3 == null) {
			return concatenate(var0, var1, var2);
		} else if (var2 == null) {
			return concatenate(var0, var1, var3);
		} else {
			return var1 == null ? concatenate(var0, var2, var3) : concatenate(var1, var2, var3);
		}
	}

	public static int[] concatenate(int[] var0, int[] var1) {
		if (var0 == null) {
			return clone(var1);
		} else if (var1 == null) {
			return clone(var0);
		} else {
			int[] var2 = new int[var0.length + var1.length];
			System.arraycopy(var0, 0, var2, 0, var0.length);
			System.arraycopy(var1, 0, var2, var0.length, var1.length);
			return var2;
		}
	}

	public static boolean constantTimeAreEqual(byte[] var0, byte[] var1) {
		boolean var2;
		if (var0 == var1) {
			var2 = true;
		} else {
			var2 = false;
			if (var0 != null) {
				var2 = false;
				if (var1 != null) {
					int var3 = var0.length;
					int var4 = var1.length;
					var2 = false;
					if (var3 == var4) {
						int var5 = 0;

						int var6;
						for (var6 = 0; var5 != var0.length; ++var5) {
							var6 |= var0[var5] ^ var1[var5];
						}

						var2 = false;
						if (var6 == 0) {
							return true;
						}
					}
				}
			}
		}

		return var2;
	}

	public static boolean contains(int[] var0, int var1) {
		for (int var2 = 0; var2 < var0.length; ++var2) {
			if (var0[var2] == var1) {
				return true;
			}
		}

		return false;
	}

	public static boolean contains(short[] var0, short var1) {
		for (int var2 = 0; var2 < var0.length; ++var2) {
			if (var0[var2] == var1) {
				return true;
			}
		}

		return false;
	}

	public static byte[] copyOf(byte[] var0, int var1) {
		byte[] var2 = new byte[var1];
		if (var1 < var0.length) {
			System.arraycopy(var0, 0, var2, 0, var1);
			return var2;
		} else {
			System.arraycopy(var0, 0, var2, 0, var0.length);
			return var2;
		}
	}

	public static char[] copyOf(char[] var0, int var1) {
		char[] var2 = new char[var1];
		if (var1 < var0.length) {
			System.arraycopy(var0, 0, var2, 0, var1);
			return var2;
		} else {
			System.arraycopy(var0, 0, var2, 0, var0.length);
			return var2;
		}
	}

	public static int[] copyOf(int[] var0, int var1) {
		int[] var2 = new int[var1];
		if (var1 < var0.length) {
			System.arraycopy(var0, 0, var2, 0, var1);
			return var2;
		} else {
			System.arraycopy(var0, 0, var2, 0, var0.length);
			return var2;
		}
	}

	public static long[] copyOf(long[] var0, int var1) {
		long[] var2 = new long[var1];
		if (var1 < var0.length) {
			System.arraycopy(var0, 0, var2, 0, var1);
			return var2;
		} else {
			System.arraycopy(var0, 0, var2, 0, var0.length);
			return var2;
		}
	}

	public static BigInteger[] copyOf(BigInteger[] var0, int var1) {
		BigInteger[] var2 = new BigInteger[var1];
		if (var1 < var0.length) {
			System.arraycopy(var0, 0, var2, 0, var1);
			return var2;
		} else {
			System.arraycopy(var0, 0, var2, 0, var0.length);
			return var2;
		}
	}

	public static byte[] copyOfRange(byte[] var0, int var1, int var2) {
		int var3 = getLength(var1, var2);
		byte[] var4 = new byte[var3];
		if (var0.length - var1 < var3) {
			System.arraycopy(var0, var1, var4, 0, var0.length - var1);
			return var4;
		} else {
			System.arraycopy(var0, var1, var4, 0, var3);
			return var4;
		}
	}

	public static int[] copyOfRange(int[] var0, int var1, int var2) {
		int var3 = getLength(var1, var2);
		int[] var4 = new int[var3];
		if (var0.length - var1 < var3) {
			System.arraycopy(var0, var1, var4, 0, var0.length - var1);
			return var4;
		} else {
			System.arraycopy(var0, var1, var4, 0, var3);
			return var4;
		}
	}

	public static long[] copyOfRange(long[] var0, int var1, int var2) {
		int var3 = getLength(var1, var2);
		long[] var4 = new long[var3];
		if (var0.length - var1 < var3) {
			System.arraycopy(var0, var1, var4, 0, var0.length - var1);
			return var4;
		} else {
			System.arraycopy(var0, var1, var4, 0, var3);
			return var4;
		}
	}

	public static BigInteger[] copyOfRange(BigInteger[] var0, int var1, int var2) {
		int var3 = getLength(var1, var2);
		BigInteger[] var4 = new BigInteger[var3];
		if (var0.length - var1 < var3) {
			System.arraycopy(var0, var1, var4, 0, var0.length - var1);
			return var4;
		} else {
			System.arraycopy(var0, var1, var4, 0, var3);
			return var4;
		}
	}

	public static void fill(byte[] var0, byte var1) {
		for (int var2 = 0; var2 < var0.length; ++var2) {
			var0[var2] = var1;
		}

	}

	public static void fill(char[] var0, char var1) {
		for (int var2 = 0; var2 < var0.length; ++var2) {
			var0[var2] = var1;
		}

	}

	public static void fill(int[] var0, int var1) {
		for (int var2 = 0; var2 < var0.length; ++var2) {
			var0[var2] = var1;
		}

	}

	public static void fill(long[] var0, long var1) {
		for (int var3 = 0; var3 < var0.length; ++var3) {
			var0[var3] = var1;
		}

	}

	public static void fill(short[] var0, short var1) {
		for (int var2 = 0; var2 < var0.length; ++var2) {
			var0[var2] = var1;
		}

	}

	private static int getLength(int var0, int var1) {
		int var2 = var1 - var0;
		if (var2 < 0) {
			StringBuffer var3 = new StringBuffer(var0);
			var3.append(" > ").append(var1);
			throw new IllegalArgumentException(var3.toString());
		} else {
			return var2;
		}
	}

	public static int hashCode(byte[] var0) {
		int var2;
		if (var0 == null) {
			var2 = 0;
		} else {
			int var1 = var0.length;
			var2 = var1 + 1;

			while (true) {
				--var1;
				if (var1 < 0) {
					break;
				}

				var2 = var2 * 257 ^ var0[var1];
			}
		}

		return var2;
	}

	public static int hashCode(byte[] var0, int var1, int var2) {
		int var3;
		if (var0 == null) {
			var3 = 0;
		} else {
			var3 = var2 + 1;

			while (true) {
				--var2;
				if (var2 < 0) {
					break;
				}

				var3 = var3 * 257 ^ var0[var1 + var2];
			}
		}

		return var3;
	}

	public static int hashCode(char[] var0) {
		int var2;
		if (var0 == null) {
			var2 = 0;
		} else {
			int var1 = var0.length;
			var2 = var1 + 1;

			while (true) {
				--var1;
				if (var1 < 0) {
					break;
				}

				var2 = var2 * 257 ^ var0[var1];
			}
		}

		return var2;
	}

	public static int hashCode(int[] var0) {
		int var2;
		if (var0 == null) {
			var2 = 0;
		} else {
			int var1 = var0.length;
			var2 = var1 + 1;

			while (true) {
				--var1;
				if (var1 < 0) {
					break;
				}

				var2 = var2 * 257 ^ var0[var1];
			}
		}

		return var2;
	}

	public static int hashCode(int[] var0, int var1, int var2) {
		int var3;
		if (var0 == null) {
			var3 = 0;
		} else {
			var3 = var2 + 1;

			while (true) {
				--var2;
				if (var2 < 0) {
					break;
				}

				var3 = var3 * 257 ^ var0[var1 + var2];
			}
		}

		return var3;
	}

	public static int hashCode(long[] var0) {
		int var2;
		if (var0 == null) {
			var2 = 0;
		} else {
			int var1 = var0.length;
			var2 = var1 + 1;

			while (true) {
				--var1;
				if (var1 < 0) {
					break;
				}

				long var3 = var0[var1];
				var2 = 257 * (var2 * 257 ^ (int) var3) ^ (int) (var3 >>> 32);
			}
		}

		return var2;
	}

	public static int hashCode(long[] var0, int var1, int var2) {
		int var3;
		if (var0 == null) {
			var3 = 0;
		} else {
			var3 = var2 + 1;

			while (true) {
				--var2;
				if (var2 < 0) {
					break;
				}

				long var4 = var0[var1 + var2];
				var3 = 257 * (var3 * 257 ^ (int) var4) ^ (int) (var4 >>> 32);
			}
		}

		return var3;
	}

	public static int hashCode(Object[] var0) {
		int var2;
		if (var0 == null) {
			var2 = 0;
		} else {
			int var1 = var0.length;
			var2 = var1 + 1;

			while (true) {
				--var1;
				if (var1 < 0) {
					break;
				}

				var2 = var2 * 257 ^ var0[var1].hashCode();
			}
		}

		return var2;
	}

	public static int hashCode(short[] var0) {
		int var2;
		if (var0 == null) {
			var2 = 0;
		} else {
			int var1 = var0.length;
			var2 = var1 + 1;

			while (true) {
				--var1;
				if (var1 < 0) {
					break;
				}

				var2 = var2 * 257 ^ 255 & var0[var1];
			}
		}

		return var2;
	}

	public static int hashCode(int[][] var0) {
		int var1 = 0;

		int var2;
		for (var2 = 0; var1 != var0.length; ++var1) {
			var2 = var2 * 257 + hashCode(var0[var1]);
		}

		return var2;
	}

	public static int hashCode(short[][] var0) {
		int var1 = 0;

		int var2;
		for (var2 = 0; var1 != var0.length; ++var1) {
			var2 = var2 * 257 + hashCode(var0[var1]);
		}

		return var2;
	}

	public static int hashCode(short[][][] var0) {
		int var1 = 0;

		int var2;
		for (var2 = 0; var1 != var0.length; ++var1) {
			var2 = var2 * 257 + hashCode(var0[var1]);
		}

		return var2;
	}

	public static byte[] prepend(byte[] var0, byte var1) {
		if (var0 == null) {
			return new byte[] { var1 };
		} else {
			int var2 = var0.length;
			byte[] var3 = new byte[var2 + 1];
			System.arraycopy(var0, 0, var3, 1, var2);
			var3[0] = var1;
			return var3;
		}
	}

	public static int[] prepend(int[] var0, int var1) {
		if (var0 == null) {
			return new int[] { var1 };
		} else {
			int var2 = var0.length;
			int[] var3 = new int[var2 + 1];
			System.arraycopy(var0, 0, var3, 1, var2);
			var3[0] = var1;
			return var3;
		}
	}

	public static short[] prepend(short[] var0, short var1) {
		if (var0 == null) {
			return new short[] { var1 };
		} else {
			int var2 = var0.length;
			short[] var3 = new short[var2 + 1];
			System.arraycopy(var0, 0, var3, 1, var2);
			var3[0] = var1;
			return var3;
		}
	}

	public static byte[] reverse(byte[] var0) {
		byte[] var3;
		if (var0 == null) {
			var3 = null;
		} else {
			int var1 = 0;
			int var2 = var0.length;
			var3 = new byte[var2];

			while (true) {
				--var2;
				if (var2 < 0) {
					break;
				}

				int var4 = var1 + 1;
				var3[var2] = var0[var1];
				var1 = var4;
			}
		}

		return var3;
	}

	public static int[] reverse(int[] var0) {
		int[] var3;
		if (var0 == null) {
			var3 = null;
		} else {
			int var1 = 0;
			int var2 = var0.length;
			var3 = new int[var2];

			while (true) {
				--var2;
				if (var2 < 0) {
					break;
				}

				int var4 = var1 + 1;
				var3[var2] = var0[var1];
				var1 = var4;
			}
		}

		return var3;
	}
}
