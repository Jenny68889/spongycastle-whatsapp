package org.spongycastle.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.AccessController;
import java.util.Vector;

public final class Strings {
	private static String LINE_SEPARATOR;

	static {
		try {
			LINE_SEPARATOR = (String) AccessController.doPrivileged(new Strings$1());
		} catch (Exception var3) {
			try {
				LINE_SEPARATOR = String.format("%n");
			} catch (Exception var2) {
				LINE_SEPARATOR = "\n";
			}
		}
	}

	public static char[] asCharArray(byte[] var0) {
		char[] var1 = new char[var0.length];

		for (int var2 = 0; var2 != var1.length; ++var2) {
			var1[var2] = (char) (255 & var0[var2]);
		}

		return var1;
	}

	public static String fromByteArray(byte[] var0) {
		return new String(asCharArray(var0));
	}

	public static String fromUTF8ByteArray(byte[] var0) {
		int var1 = 0;
		int var2 = 0;
		int var3 = 0;

		while (var3 < var0.length) {
			++var2;
			if ((240 & var0[var3]) == 240) {
				++var2;
				var3 += 4;
			} else if ((224 & var0[var3]) == 224) {
				var3 += 3;
			} else if ((192 & var0[var3]) == 192) {
				var3 += 2;
			} else {
				++var3;
			}
		}

		char[] var4 = new char[var2];

		char var6;
		int var7;
		for (int var5 = 0; var5 < var0.length; var4[var7] = var6) {
			if ((240 & var0[var5]) == 240) {
				int var8 = ((3 & var0[var5]) << 18 | (63 & var0[var5 + 1]) << 12 | (63 & var0[var5 + 2]) << 6
						| 63 & var0[var5 + 3]) - 65536;
				char var9 = (char) ('\ud800' | var8 >> 10);
				var6 = (char) ('\udc00' | var8 & 1023);
				var7 = var1 + 1;
				var4[var1] = var9;
				var5 += 4;
			} else if ((224 & var0[var5]) == 224) {
				var6 = (char) ((15 & var0[var5]) << 12 | (63 & var0[var5 + 1]) << 6 | 63 & var0[var5 + 2]);
				var5 += 3;
				var7 = var1;
			} else if ((208 & var0[var5]) == 208) {
				var6 = (char) ((31 & var0[var5]) << 6 | 63 & var0[var5 + 1]);
				var5 += 2;
				var7 = var1;
			} else if ((192 & var0[var5]) == 192) {
				var6 = (char) ((31 & var0[var5]) << 6 | 63 & var0[var5 + 1]);
				var5 += 2;
				var7 = var1;
			} else {
				var6 = (char) (255 & var0[var5]);
				++var5;
				var7 = var1;
			}

			var1 = var7 + 1;
		}

		return new String(var4);
	}

	public static String[] split(String var0, char var1) {
		Vector var2 = new Vector();
		boolean var3 = true;

		while (var3) {
			int var4 = var0.indexOf(var1);
			if (var4 > 0) {
				var2.addElement(var0.substring(0, var4));
				var0 = var0.substring(var4 + 1);
			} else {
				var2.addElement(var0);
				var3 = false;
			}
		}

		String[] var5 = new String[var2.size()];

		for (int var6 = 0; var6 != var5.length; ++var6) {
			var5[var6] = (String) var2.elementAt(var6);
		}

		return var5;
	}

	public static int toByteArray(String var0, byte[] var1, int var2) {
		int var3 = var0.length();

		for (int var4 = 0; var4 < var3; ++var4) {
			char var5 = var0.charAt(var4);
			var1[var2 + var4] = (byte) var5;
		}

		return var3;
	}

	public static byte[] toByteArray(String var0) {
		byte[] var1 = new byte[var0.length()];

		for (int var2 = 0; var2 != var1.length; ++var2) {
			var1[var2] = (byte) var0.charAt(var2);
		}

		return var1;
	}

	public static byte[] toByteArray(char[] var0) {
		byte[] var1 = new byte[var0.length];

		for (int var2 = 0; var2 != var1.length; ++var2) {
			var1[var2] = (byte) var0[var2];
		}

		return var1;
	}

	public static String toLowerCase(String var0) {
		int var1 = 0;
		char[] var2 = var0.toCharArray();

		boolean var3;
		for (var3 = false; var1 != var2.length; ++var1) {
			char var4 = var2[var1];
			if ('A' <= var4 && 'Z' >= var4) {
				var3 = true;
				var2[var1] = (char) (97 + (var4 - 65));
			}
		}

		if (var3) {
			var0 = new String(var2);
		}

		return var0;
	}

	public static void toUTF8ByteArray(char[] var0, OutputStream var1) throws IOException {
		for (int var2 = 0; var2 < var0.length; ++var2) {
			char var3 = var0[var2];
			if (var3 < 128) {
				var1.write(var3);
			} else if (var3 < 2048) {
				var1.write(192 | var3 >> 6);
				var1.write(128 | var3 & 63);
			} else if (var3 >= '\ud800' && var3 <= '\udfff') {
				if (var2 + 1 >= var0.length) {
					throw new IllegalStateException("invalid UTF-16 codepoint");
				}

				++var2;
				char var4 = var0[var2];
				if (var3 > '\udbff') {
					throw new IllegalStateException("invalid UTF-16 codepoint");
				}

				int var5 = 65536 + ((var3 & 1023) << 10 | var4 & 1023);
				var1.write(240 | var5 >> 18);
				var1.write(128 | 63 & var5 >> 12);
				var1.write(128 | 63 & var5 >> 6);
				var1.write(128 | var5 & 63);
			} else {
				var1.write(224 | var3 >> 12);
				var1.write(128 | 63 & var3 >> 6);
				var1.write(128 | var3 & 63);
			}
		}

	}

	public static byte[] toUTF8ByteArray(String var0) {
		return toUTF8ByteArray(var0.toCharArray());
	}

	public static byte[] toUTF8ByteArray(char[] var0) {
		ByteArrayOutputStream var1 = new ByteArrayOutputStream();

		try {
			toUTF8ByteArray(var0, var1);
		} catch (IOException var3) {
			throw new IllegalStateException("cannot encode string to byte array!");
		}

		return var1.toByteArray();
	}

	public static String toUpperCase(String var0) {
		int var1 = 0;
		char[] var2 = var0.toCharArray();

		boolean var3;
		for (var3 = false; var1 != var2.length; ++var1) {
			char var4 = var2[var1];
			if ('a' <= var4 && 'z' >= var4) {
				var3 = true;
				var2[var1] = (char) (65 + (var4 - 97));
			}
		}

		if (var3) {
			var0 = new String(var2);
		}

		return var0;
	}
}
