package org.spongycastle.jcajce.util;

import java.security.Provider;
import java.security.Security;
import org.spongycastle.jce.provider.BouncyCastleProvider;

public class BCJcaJceHelper extends ProviderJcaJceHelper {
	public BCJcaJceHelper() {
		super(getBouncyCastleProvider());
	}

	private static Provider getBouncyCastleProvider() {
		return (Provider) (Security.getProvider("SC") != null ? Security.getProvider("SC")
				: new BouncyCastleProvider());
	}
}
