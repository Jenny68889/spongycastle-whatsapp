package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.crypto.CipherKeyGenerator;
import org.spongycastle.jcajce.provider.symmetric.util.BaseKeyGenerator;

public class SHA384$KeyGenerator extends BaseKeyGenerator {
	public SHA384$KeyGenerator() {
		super("HMACSHA384", 384, new CipherKeyGenerator());
	}
}
