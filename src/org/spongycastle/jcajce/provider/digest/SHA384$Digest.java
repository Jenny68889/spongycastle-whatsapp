package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.crypto.digests.SHA384Digest;

public class SHA384$Digest extends BCMessageDigest implements Cloneable {
	public SHA384$Digest() {
		super(new SHA384Digest());
	}

	public Object clone() throws CloneNotSupportedException {
		SHA384$Digest var1 = (SHA384$Digest) super.clone();
		var1.digest = new SHA384Digest((SHA384Digest) this.digest);
		return var1;
	}
}
