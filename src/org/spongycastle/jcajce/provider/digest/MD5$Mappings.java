package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.jcajce.provider.config.ConfigurableProvider;

public class MD5$Mappings extends DigestAlgorithmProvider {
	private static final String PREFIX = MD5.class.getName();

	public void configure(ConfigurableProvider var1) {
		var1.addAlgorithm("MessageDigest.MD5", PREFIX + "$Digest");
		this.addHMACAlgorithm(var1, "MD5", PREFIX + "$HashMac", PREFIX + "$KeyGenerator");
	}
}
