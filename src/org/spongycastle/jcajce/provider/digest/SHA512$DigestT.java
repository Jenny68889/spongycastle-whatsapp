package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.crypto.digests.SHA512tDigest;

public class SHA512$DigestT extends BCMessageDigest implements Cloneable {
	public SHA512$DigestT(int var1) {
		super(new SHA512tDigest(var1));
	}

	public Object clone() throws CloneNotSupportedException {
		SHA512$DigestT var1 = (SHA512$DigestT) super.clone();
		var1.digest = new SHA512tDigest((SHA512tDigest) this.digest);
		return var1;
	}
}
