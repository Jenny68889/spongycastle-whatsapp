package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.crypto.CipherKeyGenerator;
import org.spongycastle.jcajce.provider.symmetric.util.BaseKeyGenerator;

public class SHA1$KeyGenerator extends BaseKeyGenerator {
	public SHA1$KeyGenerator() {
		super("HMACSHA1", 160, new CipherKeyGenerator());
	}
}
