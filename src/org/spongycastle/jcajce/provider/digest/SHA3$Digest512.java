package org.spongycastle.jcajce.provider.digest;

public class SHA3$Digest512 extends SHA3$DigestSHA3 {
	public SHA3$Digest512() {
		super(512);
	}
}
