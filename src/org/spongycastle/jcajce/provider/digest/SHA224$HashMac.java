package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.crypto.digests.SHA224Digest;
import org.spongycastle.crypto.macs.HMac;
import org.spongycastle.jcajce.provider.symmetric.util.BaseMac;

public class SHA224$HashMac extends BaseMac {
	public SHA224$HashMac() {
		super(new HMac(new SHA224Digest()));
	}
}
