package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.jcajce.provider.config.ConfigurableProvider;

public class SHA256$Mappings extends DigestAlgorithmProvider {
	private static final String PREFIX = SHA256.class.getName();

	public void configure(ConfigurableProvider var1) {
		var1.addAlgorithm("MessageDigest.SHA-256", PREFIX + "$Digest");
		var1.addAlgorithm("Alg.Alias.MessageDigest.SHA256", "SHA-256");
		var1.addAlgorithm("SecretKeyFactory.PBEWITHHMACSHA256", PREFIX + "$PBEWithMacKeyFactory");
		var1.addAlgorithm("Alg.Alias.SecretKeyFactory.PBEWITHHMACSHA-256", "PBEWITHHMACSHA256");
		this.addHMACAlgorithm(var1, "SHA256", PREFIX + "$HashMac", PREFIX + "$KeyGenerator");
	}
}
