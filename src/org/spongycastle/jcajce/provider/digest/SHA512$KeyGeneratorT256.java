package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.crypto.CipherKeyGenerator;
import org.spongycastle.jcajce.provider.symmetric.util.BaseKeyGenerator;

public class SHA512$KeyGeneratorT256 extends BaseKeyGenerator {
	public SHA512$KeyGeneratorT256() {
		super("HMACSHA512/256", 256, new CipherKeyGenerator());
	}
}
