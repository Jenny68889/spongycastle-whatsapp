package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.crypto.digests.SHA384Digest;
import org.spongycastle.crypto.macs.HMac;
import org.spongycastle.jcajce.provider.symmetric.util.BaseMac;

public class SHA384$HashMac extends BaseMac {
	public SHA384$HashMac() {
		super(new HMac(new SHA384Digest()));
	}
}
