package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.crypto.digests.SHA256Digest;

public class SHA256$Digest extends BCMessageDigest implements Cloneable {
	public SHA256$Digest() {
		super(new SHA256Digest());
	}

	public Object clone() throws CloneNotSupportedException {
		SHA256$Digest var1 = (SHA256$Digest) super.clone();
		var1.digest = new SHA256Digest((SHA256Digest) this.digest);
		return var1;
	}
}
