package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.crypto.digests.SHA1Digest;
import org.spongycastle.crypto.macs.HMac;
import org.spongycastle.jcajce.provider.symmetric.util.BaseMac;

public class SHA1$HashMac extends BaseMac {
	public SHA1$HashMac() {
		super(new HMac(new SHA1Digest()));
	}
}
