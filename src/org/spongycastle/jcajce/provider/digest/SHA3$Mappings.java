package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.jcajce.provider.config.ConfigurableProvider;

public class SHA3$Mappings extends DigestAlgorithmProvider {
	private static final String PREFIX = SHA3.class.getName();

	public void configure(ConfigurableProvider var1) {
		var1.addAlgorithm("MessageDigest.SHA3-224", PREFIX + "$Digest224");
		var1.addAlgorithm("MessageDigest.SHA3-256", PREFIX + "$Digest256");
		var1.addAlgorithm("MessageDigest.SHA3-384", PREFIX + "$Digest384");
		var1.addAlgorithm("MessageDigest.SHA3-512", PREFIX + "$Digest512");
	}
}
