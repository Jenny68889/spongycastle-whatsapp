package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.crypto.digests.SHA512Digest;

public class SHA512$Digest extends BCMessageDigest implements Cloneable {
	public SHA512$Digest() {
		super(new SHA512Digest());
	}

	public Object clone() throws CloneNotSupportedException {
		SHA512$Digest var1 = (SHA512$Digest) super.clone();
		var1.digest = new SHA512Digest((SHA512Digest) this.digest);
		return var1;
	}
}
