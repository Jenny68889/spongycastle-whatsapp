package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.jcajce.provider.config.ConfigurableProvider;

public class SHA224$Mappings extends DigestAlgorithmProvider {
	private static final String PREFIX = SHA224.class.getName();

	public void configure(ConfigurableProvider var1) {
		var1.addAlgorithm("MessageDigest.SHA-224", PREFIX + "$Digest");
		var1.addAlgorithm("Alg.Alias.MessageDigest.SHA224", "SHA-224");
		this.addHMACAlgorithm(var1, "SHA224", PREFIX + "$HashMac", PREFIX + "$KeyGenerator");
	}
}
