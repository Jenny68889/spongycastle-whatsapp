package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.crypto.CipherKeyGenerator;
import org.spongycastle.jcajce.provider.symmetric.util.BaseKeyGenerator;

public class MD5$KeyGenerator extends BaseKeyGenerator {
	public MD5$KeyGenerator() {
		super("HMACMD5", 128, new CipherKeyGenerator());
	}
}
