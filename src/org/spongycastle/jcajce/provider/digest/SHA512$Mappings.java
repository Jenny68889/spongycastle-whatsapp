package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.jcajce.provider.config.ConfigurableProvider;

public class SHA512$Mappings extends DigestAlgorithmProvider {
	private static final String PREFIX = SHA512.class.getName();

	public void configure(ConfigurableProvider var1) {
		var1.addAlgorithm("MessageDigest.SHA-512", PREFIX + "$Digest");
		var1.addAlgorithm("Alg.Alias.MessageDigest.SHA512", "SHA-512");
		var1.addAlgorithm("MessageDigest.SHA-512/224", PREFIX + "$DigestT224");
		var1.addAlgorithm("Alg.Alias.MessageDigest.SHA512/224", "SHA-512/224");
		var1.addAlgorithm("MessageDigest.SHA-512/256", PREFIX + "$DigestT256");
		var1.addAlgorithm("Alg.Alias.MessageDigest.SHA512256", "SHA-512/256");
		var1.addAlgorithm("Mac.OLDHMACSHA512", PREFIX + "$OldSHA512");
		this.addHMACAlgorithm(var1, "SHA512", PREFIX + "$HashMac", PREFIX + "$KeyGenerator");
		this.addHMACAlgorithm(var1, "SHA512/224", PREFIX + "$HashMacT224", PREFIX + "$KeyGeneratorT224");
		this.addHMACAlgorithm(var1, "SHA512/256", PREFIX + "$HashMacT256", PREFIX + "$KeyGeneratorT256");
	}
}
