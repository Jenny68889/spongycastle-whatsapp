package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.crypto.digests.SHA3Digest;

public class SHA3$DigestSHA3 extends BCMessageDigest implements Cloneable {
	public SHA3$DigestSHA3(int var1) {
		super(new SHA3Digest(var1));
	}

	public Object clone() throws CloneNotSupportedException {
		BCMessageDigest var1 = (BCMessageDigest) super.clone();
		var1.digest = new SHA3Digest((SHA3Digest) this.digest);
		return var1;
	}
}
