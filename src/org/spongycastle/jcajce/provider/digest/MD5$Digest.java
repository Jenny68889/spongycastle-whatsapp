package org.spongycastle.jcajce.provider.digest;

import org.spongycastle.crypto.digests.MD5Digest;

public class MD5$Digest extends BCMessageDigest implements Cloneable {
	public MD5$Digest() {
		super(new MD5Digest());
	}

	public Object clone() throws CloneNotSupportedException {
		MD5$Digest var1 = (MD5$Digest) super.clone();
		var1.digest = new MD5Digest((MD5Digest) this.digest);
		return var1;
	}
}
