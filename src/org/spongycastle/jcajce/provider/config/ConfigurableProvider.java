package org.spongycastle.jcajce.provider.config;

public interface ConfigurableProvider {
	String DH_DEFAULT_PARAMS = "DhDefaultParams";
	String EC_IMPLICITLY_CA = "ecImplicitlyCa";
	String THREAD_LOCAL_DH_DEFAULT_PARAMS = "threadLocalDhDefaultParams";
	String THREAD_LOCAL_EC_IMPLICITLY_CA = "threadLocalEcImplicitlyCa";

	void addAlgorithm(String var1, String var2);

	boolean hasAlgorithm(String var1, String var2);

	void setParameter(String var1, Object var2);
}
