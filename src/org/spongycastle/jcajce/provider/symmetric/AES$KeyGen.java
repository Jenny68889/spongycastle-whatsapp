package org.spongycastle.jcajce.provider.symmetric;

import org.spongycastle.crypto.CipherKeyGenerator;
import org.spongycastle.jcajce.provider.symmetric.util.BaseKeyGenerator;

public class AES$KeyGen extends BaseKeyGenerator {
	public AES$KeyGen() {
		this(192);
	}

	public AES$KeyGen(int var1) {
		super("AES", var1, new CipherKeyGenerator());
	}
}
