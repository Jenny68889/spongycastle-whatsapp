package org.spongycastle.jcajce.provider.symmetric;

import org.spongycastle.crypto.BufferedBlockCipher;
import org.spongycastle.crypto.engines.AESFastEngine;
import org.spongycastle.crypto.modes.OFBBlockCipher;
import org.spongycastle.jcajce.provider.symmetric.util.BaseBlockCipher;

public class AES$OFB extends BaseBlockCipher {
	public AES$OFB() {
		super((BufferedBlockCipher) (new BufferedBlockCipher(new OFBBlockCipher(new AESFastEngine(), 128))), 128);
	}
}
