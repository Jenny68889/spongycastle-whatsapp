package org.spongycastle.jcajce.provider.symmetric;

import org.spongycastle.crypto.CipherKeyGenerator;
import org.spongycastle.jcajce.provider.symmetric.util.BaseKeyGenerator;

public class ARC4$KeyGen extends BaseKeyGenerator {
	public ARC4$KeyGen() {
		super("RC4", 128, new CipherKeyGenerator());
	}
}
