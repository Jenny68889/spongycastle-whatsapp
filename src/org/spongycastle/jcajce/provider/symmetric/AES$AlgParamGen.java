package org.spongycastle.jcajce.provider.symmetric;

import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import org.spongycastle.jcajce.provider.symmetric.util.BaseAlgorithmParameterGenerator;

public class AES$AlgParamGen extends BaseAlgorithmParameterGenerator {
	protected AlgorithmParameters engineGenerateParameters() {
		byte[] var1 = new byte[16];
		if (this.random == null) {
			this.random = new SecureRandom();
		}

		this.random.nextBytes(var1);

		try {
			AlgorithmParameters var3 = this.createParametersInstance("AES");
			var3.init(new IvParameterSpec(var1));
			return var3;
		} catch (Exception var4) {
			throw new RuntimeException(var4.getMessage());
		}
	}

	protected void engineInit(AlgorithmParameterSpec var1, SecureRandom var2)
			throws InvalidAlgorithmParameterException {
		throw new InvalidAlgorithmParameterException(
				"No supported AlgorithmParameterSpec for AES parameter generation.");
	}
}
