package org.spongycastle.jcajce.provider.symmetric.util;

import java.io.IOException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidParameterSpecException;
import javax.crypto.spec.IvParameterSpec;
import org.spongycastle.util.Arrays;

public class IvAlgorithmParameters extends BaseAlgorithmParameters {
	private byte[] iv;

	protected byte[] engineGetEncoded() {
		return this.engineGetEncoded("ASN.1");
	}

	protected byte[] engineGetEncoded(String var1) {
		return var1.equals("RAW") ? Arrays.clone(this.iv) : null;
	}

	protected void engineInit(AlgorithmParameterSpec var1) throws InvalidParameterSpecException {
		if (!(var1 instanceof IvParameterSpec)) {
			throw new InvalidParameterSpecException(
					"IvParameterSpec required to initialise a IV parameters algorithm parameters object");
		} else {
			this.iv = ((IvParameterSpec) var1).getIV();
		}
	}

	protected void engineInit(byte[] var1) {
		this.iv = Arrays.clone(var1);
	}

	protected void engineInit(byte[] var1, String var2) throws IOException {
		if (var2.equals("RAW")) {
			this.engineInit(var1);
		} else {
			throw new IOException("Unknown parameters format in IV parameters object");
		}
	}

	protected String engineToString() {
		return "IV Parameters";
	}

	protected AlgorithmParameterSpec localEngineGetParameterSpec(Class var1) throws InvalidParameterSpecException {
		if (var1 == IvParameterSpec.class) {
			return new IvParameterSpec(this.iv);
		} else {
			throw new InvalidParameterSpecException("unknown parameter spec passed to IV parameters object.");
		}
	}
}
