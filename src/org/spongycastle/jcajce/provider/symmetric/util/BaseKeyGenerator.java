package org.spongycastle.jcajce.provider.symmetric.util;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidParameterException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.KeyGeneratorSpi;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.spongycastle.crypto.CipherKeyGenerator;
import org.spongycastle.crypto.KeyGenerationParameters;

public class BaseKeyGenerator extends KeyGeneratorSpi {
	protected String algName;
	protected int defaultKeySize;
	protected CipherKeyGenerator engine;
	protected int keySize;
	protected boolean uninitialised = true;

	protected BaseKeyGenerator(String var1, int var2, CipherKeyGenerator var3) {
		this.algName = var1;
		this.defaultKeySize = var2;
		this.keySize = var2;
		this.engine = var3;
	}

	protected SecretKey engineGenerateKey() {
		if (this.uninitialised) {
			this.engine.init(new KeyGenerationParameters(new SecureRandom(), this.defaultKeySize));
			this.uninitialised = false;
		}

		return new SecretKeySpec(this.engine.generateKey(), this.algName);
	}

	protected void engineInit(int var1, SecureRandom var2) {
		IllegalArgumentException var10000;
		label19: {
			boolean var10001;
			if (var2 == null) {
				try {
					var2 = new SecureRandom();
				} catch (IllegalArgumentException var5) {
					var10000 = var5;
					var10001 = false;
					break label19;
				}
			}

			try {
				this.engine.init(new KeyGenerationParameters(var2, var1));
				this.uninitialised = false;
				return;
			} catch (IllegalArgumentException var4) {
				var10000 = var4;
				var10001 = false;
			}
		}

		IllegalArgumentException var3 = var10000;
		throw new InvalidParameterException(var3.getMessage());
	}

	protected void engineInit(SecureRandom var1) {
		if (var1 != null) {
			this.engine.init(new KeyGenerationParameters(var1, this.defaultKeySize));
			this.uninitialised = false;
		}

	}

	protected void engineInit(AlgorithmParameterSpec var1, SecureRandom var2) throws InvalidAlgorithmParameterException {
		throw new InvalidAlgorithmParameterException("Not Implemented");
	}
}
