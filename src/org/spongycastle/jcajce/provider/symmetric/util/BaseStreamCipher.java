package org.spongycastle.jcajce.provider.symmetric.util;

import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.InvalidParameterException;
import java.security.Key;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.RC2ParameterSpec;
import javax.crypto.spec.RC5ParameterSpec;
import org.spongycastle.crypto.CipherParameters;
import org.spongycastle.crypto.DataLengthException;
import org.spongycastle.crypto.StreamCipher;
import org.spongycastle.crypto.params.KeyParameter;
import org.spongycastle.crypto.params.ParametersWithIV;

public class BaseStreamCipher extends BaseWrapCipher implements PBE {
	private Class[] availableSpecs;
	private StreamCipher cipher;
	private int digest;
	private int ivLength;
	private ParametersWithIV ivParam;
	private int keySizeInBits;
	private String pbeAlgorithm;
	private PBEParameterSpec pbeSpec;

	protected BaseStreamCipher(StreamCipher var1, int var2) {
		this(var1, var2, -1, -1);
	}

	protected BaseStreamCipher(StreamCipher var1, int var2, int var3, int var4) {
		this.availableSpecs = new Class[] { RC2ParameterSpec.class, RC5ParameterSpec.class, IvParameterSpec.class,
				PBEParameterSpec.class };
		this.ivLength = 0;
		this.pbeSpec = null;
		this.pbeAlgorithm = null;
		this.cipher = var1;
		this.ivLength = var2;
		this.keySizeInBits = var3;
		this.digest = var4;
	}

	protected int engineDoFinal(byte[] var1, int var2, int var3, byte[] var4, int var5) throws ShortBufferException {
		if (var5 + var3 > var4.length) {
			throw new ShortBufferException("output buffer too short for input.");
		} else {
			if (var3 != 0) {
				this.cipher.processBytes(var1, var2, var3, var4, var5);
			}

			this.cipher.reset();
			return var3;
		}
	}

	protected byte[] engineDoFinal(byte[] var1, int var2, int var3) {
		if (var3 != 0) {
			byte[] var4 = this.engineUpdate(var1, var2, var3);
			this.cipher.reset();
			return var4;
		} else {
			this.cipher.reset();
			return new byte[0];
		}
	}

	protected int engineGetBlockSize() {
		return 0;
	}

	protected byte[] engineGetIV() {
		return this.ivParam != null ? this.ivParam.getIV() : null;
	}

	protected int engineGetKeySize(Key var1) {
		return 8 * var1.getEncoded().length;
	}

	protected int engineGetOutputSize(int var1) {
		return var1;
	}

	protected AlgorithmParameters engineGetParameters() {
		if (this.engineParams == null && this.pbeSpec != null) {
			try {
				AlgorithmParameters var2 = this.createParametersInstance(this.pbeAlgorithm);
				var2.init(this.pbeSpec);
				return var2;
			} catch (Exception var3) {
				return null;
			}
		} else {
			return this.engineParams;
		}
	}

	protected void engineInit(int var1, Key var2, AlgorithmParameters var3, SecureRandom var4) throws InvalidAlgorithmParameterException, InvalidKeyException {
		AlgorithmParameterSpec var5;
		if (var3 != null) {
			int var6 = 0;

			while (true) {
				if (var6 == this.availableSpecs.length) {
					var5 = null;
					break;
				}

				AlgorithmParameterSpec var8;
				try {
					var8 = var3.getParameterSpec(this.availableSpecs[var6]);
				} catch (Exception var9) {
					++var6;
					continue;
				}

				var5 = var8;
				break;
			}

			if (var5 == null) {
				throw new InvalidAlgorithmParameterException("can't handle parameter " + var3.toString());
			}
		} else {
			var5 = null;
		}

		this.engineInit(var1, var2, var5, var4);
		this.engineParams = var3;
	}

	protected void engineInit(int var1, Key var2, SecureRandom var3) throws InvalidKeyException {
		try {
			this.engineInit(var1, var2, (AlgorithmParameterSpec) null, var3);
		} catch (InvalidAlgorithmParameterException var5) {
			throw new InvalidKeyException(var5.getMessage());
		}
	}

	protected void engineInit(int var1, Key var2, AlgorithmParameterSpec var3, SecureRandom var4) throws InvalidKeyException, InvalidAlgorithmParameterException {
		this.pbeSpec = null;
		this.pbeAlgorithm = null;
		this.engineParams = null;
		if (!(var2 instanceof SecretKey)) {
			throw new InvalidKeyException(
					"Key for algorithm " + var2.getAlgorithm() + " not suitable for symmetric enryption.");
		} else {
			Object var6;
			if (var2 instanceof BCPBEKey) {
				BCPBEKey var10 = (BCPBEKey) var2;
				this.pbeAlgorithm = var10.getAlgorithm();
				CipherParameters var11;
				if (var10.getParam() != null) {
					var11 = var10.getParam();
					this.pbeSpec = new PBEParameterSpec(var10.getSalt(), var10.getIterationCount());
				} else {
					if (!(var3 instanceof PBEParameterSpec)) {
						throw new InvalidAlgorithmParameterException("PBE requires PBE parameters to be set.");
					}

					var11 = PBE$Util.makePBEParameters(var10, var3, this.cipher.getAlgorithmName());
					this.pbeSpec = (PBEParameterSpec) var3;
				}

				if (var10.getIvSize() != 0) {
					this.ivParam = (ParametersWithIV) var11;
					var6 = var11;
				} else {
					var6 = var11;
				}
			} else if (var3 == null) {
				if (this.digest > 0) {
					throw new InvalidKeyException("Algorithm requires a PBE key");
				}

				var6 = new KeyParameter(var2.getEncoded());
			} else {
				if (!(var3 instanceof IvParameterSpec)) {
					throw new InvalidAlgorithmParameterException("unknown parameter type.");
				}

				ParametersWithIV var5 = new ParametersWithIV(new KeyParameter(var2.getEncoded()),
						((IvParameterSpec) var3).getIV());
				this.ivParam = (ParametersWithIV) var5;
				var6 = var5;
			}

			if (this.ivLength != 0 && !(var6 instanceof ParametersWithIV)) {
				if (var4 == null) {
					var4 = new SecureRandom();
				}

				if (var1 != 1 && var1 != 3) {
					throw new InvalidAlgorithmParameterException("no IV set when one expected");
				}

				byte[] var8 = new byte[this.ivLength];
				var4.nextBytes(var8);
				ParametersWithIV var9 = new ParametersWithIV((CipherParameters) var6, var8);
				this.ivParam = (ParametersWithIV) var9;
				var6 = var9;
			}

			Exception var10000;
			boolean var10001;
			switch (var1) {
			case 1:
			case 3:
				try {
					this.cipher.init(true, (CipherParameters) var6);
					return;
				} catch (Exception var13) {
					var10000 = var13;
					var10001 = false;
					break;
				}
			case 2:
			case 4:
				try {
					this.cipher.init(false, (CipherParameters) var6);
					return;
				} catch (Exception var12) {
					var10000 = var12;
					var10001 = false;
					break;
				}
			default:
				try {
					throw new InvalidParameterException("unknown opmode " + var1 + " passed");
				} catch (Exception var14) {
					var10000 = var14;
					var10001 = false;
				}
			}

			Exception var7 = var10000;
			throw new InvalidKeyException(var7.getMessage());
		}
	}

	protected void engineSetMode(String var1) {
		if (!var1.equalsIgnoreCase("ECB")) {
			throw new IllegalArgumentException("can't support mode " + var1);
		}
	}

	protected void engineSetPadding(String var1) throws NoSuchPaddingException {
		if (!var1.equalsIgnoreCase("NoPadding")) {
			throw new NoSuchPaddingException("Padding " + var1 + " unknown.");
		}
	}

	protected int engineUpdate(byte[] var1, int var2, int var3, byte[] var4, int var5) throws ShortBufferException {
		if (var5 + var3 > var4.length) {
			throw new ShortBufferException("output buffer too short for input.");
		} else {
			try {
				this.cipher.processBytes(var1, var2, var3, var4, var5);
				return var3;
			} catch (DataLengthException var7) {
				throw new IllegalStateException(var7.getMessage());
			}
		}
	}

	protected byte[] engineUpdate(byte[] var1, int var2, int var3) {
		byte[] var4 = new byte[var3];
		this.cipher.processBytes(var1, var2, var3, var4, 0);
		return var4;
	}
}
