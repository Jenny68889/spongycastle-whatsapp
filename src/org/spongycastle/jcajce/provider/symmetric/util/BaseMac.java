package org.spongycastle.jcajce.provider.symmetric.util;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.crypto.MacSpi;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEParameterSpec;
import org.spongycastle.crypto.CipherParameters;
import org.spongycastle.crypto.Mac;
import org.spongycastle.crypto.params.KeyParameter;
import org.spongycastle.crypto.params.ParametersWithIV;

public class BaseMac extends MacSpi implements PBE {
	private int keySize = 160;
	private Mac macEngine;
	private int pbeHash = 1;
	private int scheme = 2;

	protected BaseMac(Mac var1) {
		this.macEngine = var1;
	}

	protected BaseMac(Mac var1, int var2, int var3, int var4) {
		this.macEngine = var1;
		this.scheme = var2;
		this.pbeHash = var3;
		this.keySize = var4;
	}

	private static Hashtable copyMap(Map var0) {
		Hashtable var1 = new Hashtable();
		Iterator var2 = var0.keySet().iterator();

		while (var2.hasNext()) {
			Object var3 = var2.next();
			var1.put(var3, var0.get(var3));
		}

		return var1;
	}

	protected byte[] engineDoFinal() {
		byte[] var1 = new byte[this.engineGetMacLength()];
		this.macEngine.doFinal(var1, 0);
		return var1;
	}

	protected int engineGetMacLength() {
		return this.macEngine.getMacSize();
	}

	protected void engineInit(Key var1, AlgorithmParameterSpec var2)
			throws InvalidKeyException, InvalidAlgorithmParameterException {
		if (var1 == null) {
			throw new InvalidKeyException("key is null");
		} else {
			Object var3;
			if (var1 instanceof BCPBEKey) {
				BCPBEKey var4 = (BCPBEKey) var1;
				if (var4.getParam() != null) {
					var3 = var4.getParam();
				} else {
					if (!(var2 instanceof PBEParameterSpec)) {
						throw new InvalidAlgorithmParameterException("PBE requires PBE parameters to be set.");
					}

					var3 = PBE$Util.makePBEMacParameters(var4, var2);
				}
			} else if (var2 instanceof IvParameterSpec) {
				var3 = new ParametersWithIV(new KeyParameter(var1.getEncoded()), ((IvParameterSpec) var2).getIV());
			} else {
				if (var2 != null) {
					throw new InvalidAlgorithmParameterException("unknown parameter type.");
				}

				var3 = new KeyParameter(var1.getEncoded());
			}

			this.macEngine.init((CipherParameters) var3);
		}
	}

	protected void engineReset() {
		this.macEngine.reset();
	}

	protected void engineUpdate(byte var1) {
		this.macEngine.update(var1);
	}

	protected void engineUpdate(byte[] var1, int var2, int var3) {
		this.macEngine.update(var1, var2, var3);
	}
}
