package org.spongycastle.jcajce.provider.symmetric.util;

import javax.crypto.BadPaddingException;
import org.spongycastle.crypto.BlockCipher;
import org.spongycastle.crypto.BufferedBlockCipher;
import org.spongycastle.crypto.CipherParameters;
import org.spongycastle.crypto.CryptoException;
import org.spongycastle.crypto.InvalidCipherTextException;
import org.spongycastle.crypto.paddings.BlockCipherPadding;
import org.spongycastle.crypto.paddings.PaddedBufferedBlockCipher;

class BaseBlockCipher$BufferedGenericBlockCipher implements BaseBlockCipher$GenericBlockCipher {
	private BufferedBlockCipher cipher;

	BaseBlockCipher$BufferedGenericBlockCipher(BlockCipher var1) {
		this.cipher = new PaddedBufferedBlockCipher(var1);
	}

	BaseBlockCipher$BufferedGenericBlockCipher(BlockCipher var1, BlockCipherPadding var2) {
		this.cipher = new PaddedBufferedBlockCipher(var1, var2);
	}

	BaseBlockCipher$BufferedGenericBlockCipher(BufferedBlockCipher var1) {
		this.cipher = var1;
	}

	public int doFinal(byte[] var1, int var2) throws CryptoException, BadPaddingException {
		try {
			int var4 = this.cipher.doFinal(var1, var2);
			return var4;
		} catch (InvalidCipherTextException var5) {
			throw new BadPaddingException(var5.getMessage());
		}
	}

	public String getAlgorithmName() {
		return this.cipher.getUnderlyingCipher().getAlgorithmName();
	}

	public int getOutputSize(int var1) {
		return this.cipher.getOutputSize(var1);
	}

	public BlockCipher getUnderlyingCipher() {
		return this.cipher.getUnderlyingCipher();
	}

	public int getUpdateOutputSize(int var1) {
		return this.cipher.getUpdateOutputSize(var1);
	}

	public void init(boolean var1, CipherParameters var2) {
		this.cipher.init(var1, var2);
	}

	public int processByte(byte var1, byte[] var2, int var3) {
		return this.cipher.processByte(var1, var2, var3);
	}

	public int processBytes(byte[] var1, int var2, int var3, byte[] var4, int var5) {
		return this.cipher.processBytes(var1, var2, var3, var4, var5);
	}

	public void updateAAD(byte[] var1, int var2, int var3) {
		throw new UnsupportedOperationException("AAD is not supported in the current mode.");
	}

	public boolean wrapOnNoPadding() {
		return false;
	}
}
