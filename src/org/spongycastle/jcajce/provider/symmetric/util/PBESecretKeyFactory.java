package org.spongycastle.jcajce.provider.symmetric.util;

import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import javax.crypto.SecretKey;
import javax.crypto.spec.PBEKeySpec;
import org.spongycastle.crypto.CipherParameters;

public class PBESecretKeyFactory extends BaseSecretKeyFactory implements PBE {
	private int digest;
	private boolean forCipher;
	private int ivSize;
	private int keySize;
	private int scheme;

	public PBESecretKeyFactory(String var1, boolean var2, int var3, int var4, int var5, int var6) {
		super(var1);
		this.forCipher = var2;
		this.scheme = var3;
		this.digest = var4;
		this.keySize = var5;
		this.ivSize = var6;
	}

	protected SecretKey engineGenerateSecret(KeySpec var1) throws InvalidKeySpecException {
		if (var1 instanceof PBEKeySpec) {
			PBEKeySpec var2 = (PBEKeySpec) var1;
			if (var2.getSalt() == null) {
				return new BCPBEKey(this.algName, this.scheme, this.digest, this.keySize, this.ivSize, var2,
						(CipherParameters) null);
			} else {
				CipherParameters var3;
				if (this.forCipher) {
					var3 = PBE$Util.makePBEParameters(var2, this.scheme, this.digest, this.keySize, this.ivSize);
				} else {
					var3 = PBE$Util.makePBEMacParameters(var2, this.scheme, this.digest, this.keySize);
				}

				return new BCPBEKey(this.algName, this.scheme, this.digest, this.keySize, this.ivSize, var2, var3);
			}
		} else {
			throw new InvalidKeySpecException("Invalid KeySpec");
		}
	}
}
