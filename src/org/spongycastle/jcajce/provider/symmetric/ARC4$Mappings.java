package org.spongycastle.jcajce.provider.symmetric;

import org.spongycastle.jcajce.provider.config.ConfigurableProvider;
import org.spongycastle.jcajce.provider.util.AlgorithmProvider;

public class ARC4$Mappings extends AlgorithmProvider {
	private static final String PREFIX = ARC4.class.getName();

	public void configure(ConfigurableProvider var1) {
		var1.addAlgorithm("Cipher.ARC4", PREFIX + "$Base");
		var1.addAlgorithm("Alg.Alias.Cipher.ARCFOUR", "ARC4");
		var1.addAlgorithm("Alg.Alias.Cipher.RC4", "ARC4");
		var1.addAlgorithm("KeyGenerator.ARC4", PREFIX + "$KeyGen");
		var1.addAlgorithm("Alg.Alias.KeyGenerator.RC4", "ARC4");
		var1.addAlgorithm("Alg.Alias.KeyGenerator.1.2.840.113549.3.4", "ARC4");
		var1.addAlgorithm("SecretKeyFactory.PBEWITHSHAAND128BITRC4", PREFIX + "$PBEWithSHAAnd128BitKeyFactory");
		var1.addAlgorithm("SecretKeyFactory.PBEWITHSHAAND40BITRC4", PREFIX + "$PBEWithSHAAnd40BitKeyFactory");
		var1.addAlgorithm("Alg.Alias.AlgorithmParameters.PBEWITHSHAAND40BITRC4", "PKCS12PBE");
		var1.addAlgorithm("Alg.Alias.AlgorithmParameters.PBEWITHSHAAND128BITRC4", "PKCS12PBE");
		var1.addAlgorithm("Alg.Alias.AlgorithmParameters.PBEWITHSHAANDRC4", "PKCS12PBE");
		var1.addAlgorithm("Cipher.PBEWITHSHAAND128BITRC4", PREFIX + "$PBEWithSHAAnd128Bit");
		var1.addAlgorithm("Cipher.PBEWITHSHAAND40BITRC4", PREFIX + "$PBEWithSHAAnd40Bit");
		var1.addAlgorithm("Alg.Alias.Cipher.PBEWITHSHA1AND128BITRC4", "PBEWITHSHAAND128BITRC4");
		var1.addAlgorithm("Alg.Alias.Cipher.PBEWITHSHA1AND40BITRC4", "PBEWITHSHAAND40BITRC4");
	}
}
