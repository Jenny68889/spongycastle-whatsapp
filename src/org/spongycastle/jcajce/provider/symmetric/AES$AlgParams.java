package org.spongycastle.jcajce.provider.symmetric;

import org.spongycastle.jcajce.provider.symmetric.util.IvAlgorithmParameters;

public class AES$AlgParams extends IvAlgorithmParameters {
	protected String engineToString() {
		return "AES IV";
	}
}
