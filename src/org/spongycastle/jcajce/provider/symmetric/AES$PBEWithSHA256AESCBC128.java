package org.spongycastle.jcajce.provider.symmetric;

import org.spongycastle.crypto.engines.AESFastEngine;
import org.spongycastle.crypto.modes.CBCBlockCipher;
import org.spongycastle.jcajce.provider.symmetric.util.BaseBlockCipher;

public class AES$PBEWithSHA256AESCBC128 extends BaseBlockCipher {
	public AES$PBEWithSHA256AESCBC128() {
		super(new CBCBlockCipher(new AESFastEngine()), 2, 4, 128, 16);
	}
}
