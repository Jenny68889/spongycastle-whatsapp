package org.spongycastle.jcajce.provider.symmetric;

import org.spongycastle.crypto.engines.AESFastEngine;
import org.spongycastle.crypto.modes.CBCBlockCipher;
import org.spongycastle.jcajce.provider.symmetric.util.BaseBlockCipher;

public class AES$PBEWithSHA1AESCBC192 extends BaseBlockCipher {
	public AES$PBEWithSHA1AESCBC192() {
		super(new CBCBlockCipher(new AESFastEngine()), 2, 1, 192, 16);
	}
}
