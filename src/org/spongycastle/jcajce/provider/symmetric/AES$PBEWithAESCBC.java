package org.spongycastle.jcajce.provider.symmetric;

import org.spongycastle.crypto.BlockCipher;
import org.spongycastle.crypto.engines.AESFastEngine;
import org.spongycastle.crypto.modes.CBCBlockCipher;
import org.spongycastle.jcajce.provider.symmetric.util.BaseBlockCipher;

public class AES$PBEWithAESCBC extends BaseBlockCipher {
	public AES$PBEWithAESCBC() {
		super((BlockCipher) (new CBCBlockCipher(new AESFastEngine())));
	}
}
