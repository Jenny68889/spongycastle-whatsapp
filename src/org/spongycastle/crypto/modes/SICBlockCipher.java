package org.spongycastle.crypto.modes;

import org.spongycastle.crypto.BlockCipher;
import org.spongycastle.crypto.CipherParameters;
import org.spongycastle.crypto.StreamBlockCipher;
import org.spongycastle.crypto.params.ParametersWithIV;
import org.spongycastle.util.Arrays;
import org.spongycastle.util.Pack;

public class SICBlockCipher extends StreamBlockCipher {
	private byte[] IV;
	private final int blockSize;
	private int byteCount;
	private final BlockCipher cipher;
	private byte[] counter;
	private byte[] counterOut;

	public SICBlockCipher(BlockCipher var1) {
		super(var1);
		this.cipher = var1;
		this.blockSize = this.cipher.getBlockSize();
		this.IV = new byte[this.blockSize];
		this.counter = new byte[this.blockSize];
		this.counterOut = new byte[this.blockSize];
		this.byteCount = 0;
	}

	private void adjustCounter(long var1) {
		if (var1 >= 0L) {
			long var13 = (var1 + (long) this.byteCount) / (long) this.blockSize;
			long var15;
			if (var13 > 255L) {
				int var17 = 5;

				for (var15 = var13; var17 >= 1; --var17) {
					for (long var18 = 1L << var17 * 8; var15 >= var18; var15 -= var18) {
						this.incrementCounterAt(var17);
					}
				}
			} else {
				var15 = var13;
			}

			this.incrementCounter((int) var15);
			this.byteCount = (int) (var1 + (long) this.byteCount - var13 * (long) this.blockSize);
		} else {
			long var3 = (-var1 - (long) this.byteCount) / (long) this.blockSize;
			long var5;
			if (var3 > 255L) {
				int var10 = 5;

				for (var5 = var3; var10 >= 1; --var10) {
					for (long var11 = 1L << var10 * 8; var5 > var11; var5 -= var11) {
						this.decrementCounterAt(var10);
					}
				}
			} else {
				var5 = var3;
			}

			for (long var7 = 0L; var7 != var5; ++var7) {
				this.decrementCounterAt(0);
			}

			int var9 = (int) (var1 + (long) this.byteCount + var3 * (long) this.blockSize);
			if (var9 >= 0) {
				this.byteCount = 0;
			} else {
				this.decrementCounterAt(0);
				this.byteCount = var9 + this.blockSize;
			}
		}
	}

	private void checkCounter() {
		if (this.IV.length < this.blockSize) {
			for (int var1 = 0; var1 != this.IV.length; ++var1) {
				if (this.counter[var1] != this.IV[var1]) {
					throw new IllegalStateException("Counter in CTR/SIC mode out of range.");
				}
			}
		}

	}

	private void decrementCounterAt(int var1) {
		int var2 = this.counter.length - var1;

		byte var4;
		do {
			--var2;
			if (var2 < 0) {
				return;
			}

			byte[] var3 = this.counter;
			var4 = (byte) (-1 + var3[var2]);
			var3[var2] = var4;
		} while (var4 == -1);

	}

	private void incrementCounter(int var1) {
		byte var2 = this.counter[-1 + this.counter.length];
		byte[] var3 = this.counter;
		int var4 = -1 + this.counter.length;
		var3[var4] = (byte) (var1 + var3[var4]);
		if (var2 != 0 && this.counter[-1 + this.counter.length] < var2) {
			this.incrementCounterAt(1);
		}

	}

	private void incrementCounterAt(int var1) {
		int var2 = this.counter.length - var1;

		byte var4;
		do {
			--var2;
			if (var2 < 0) {
				return;
			}

			byte[] var3 = this.counter;
			var4 = (byte) (1 + var3[var2]);
			var3[var2] = var4;
		} while (var4 == 0);

	}

	protected byte calculateByte(byte var1) {
		byte var4;
		if (this.byteCount == 0) {
			this.cipher.processBlock(this.counter, 0, this.counterOut, 0);
			byte[] var6 = this.counterOut;
			int var7 = this.byteCount++;
			var4 = (byte) (var1 ^ var6[var7]);
		} else {
			byte[] var2 = this.counterOut;
			int var3 = this.byteCount++;
			var4 = (byte) (var1 ^ var2[var3]);
			if (this.byteCount == this.counter.length) {
				this.byteCount = 0;
				this.incrementCounterAt(0);
				this.checkCounter();
				return var4;
			}
		}

		return var4;
	}

	public String getAlgorithmName() {
		return this.cipher.getAlgorithmName() + "/SIC";
	}

	public int getBlockSize() {
		return this.cipher.getBlockSize();
	}

	public long getPosition() {
		byte[] var1 = new byte[this.counter.length];
		System.arraycopy(this.counter, 0, var1, 0, var1.length);

		for (int var2 = -1 + var1.length; var2 >= 1; --var2) {
			int var3;
			if (var2 < this.IV.length) {
				var3 = (255 & var1[var2]) - (255 & this.IV[var2]);
			} else {
				var3 = 255 & var1[var2];
			}

			if (var3 < 0) {
				int var4 = var2 - 1;
				var1[var4] += -1;
				var3 += 256;
			}

			var1[var2] = (byte) var3;
		}

		return Pack.bigEndianToLong(var1, -8 + var1.length) * (long) this.blockSize + (long) this.byteCount;
	}

	public void init(boolean var1, CipherParameters var2) {
		int var3 = 8;
		if (var2 instanceof ParametersWithIV) {
			ParametersWithIV var4 = (ParametersWithIV) var2;
			this.IV = Arrays.clone(var4.getIV());
			if (this.blockSize < this.IV.length) {
				throw new IllegalArgumentException(
						"CTR/SIC mode requires IV no greater than: " + this.blockSize + " bytes.");
			} else {
				if (var3 > this.blockSize / 2) {
					var3 = this.blockSize / 2;
				}

				if (this.blockSize - this.IV.length > var3) {
					throw new IllegalArgumentException(
							"CTR/SIC mode requires IV of at least: " + (this.blockSize - var3) + " bytes.");
				} else {
					if (var4.getParameters() != null) {
						this.cipher.init(true, var4.getParameters());
					}

					this.reset();
				}
			}
		} else {
			throw new IllegalArgumentException("CTR/SIC mode requires ParametersWithIV");
		}
	}

	public int processBlock(byte[] var1, int var2, byte[] var3, int var4) {
		this.processBytes(var1, var2, this.blockSize, var3, var4);
		return this.blockSize;
	}

	public void reset() {
		Arrays.fill((byte[]) this.counter, (byte) 0);
		System.arraycopy(this.IV, 0, this.counter, 0, this.IV.length);
		this.cipher.reset();
		this.byteCount = 0;
	}

	public long seekTo(long var1) {
		this.reset();
		return this.skip(var1);
	}

	public long skip(long var1) {
		this.adjustCounter(var1);
		this.checkCounter();
		this.cipher.processBlock(this.counter, 0, this.counterOut, 0);
		return var1;
	}
}
