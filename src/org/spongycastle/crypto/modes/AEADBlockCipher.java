package org.spongycastle.crypto.modes;

import org.spongycastle.crypto.BlockCipher;
import org.spongycastle.crypto.CipherParameters;
import org.spongycastle.crypto.CryptoException;

public interface AEADBlockCipher {
	int doFinal(byte[] var1, int var2) throws CryptoException;

	String getAlgorithmName();

	byte[] getMac();

	int getOutputSize(int var1);

	BlockCipher getUnderlyingCipher();

	int getUpdateOutputSize(int var1);

	void init(boolean var1, CipherParameters var2);

	void processAADByte(byte var1);

	void processAADBytes(byte[] var1, int var2, int var3);

	int processByte(byte var1, byte[] var2, int var3);

	int processBytes(byte[] var1, int var2, int var3, byte[] var4, int var5);

	void reset();
}
