package org.spongycastle.crypto;

public interface StreamCipher {
	String getAlgorithmName();

	void init(boolean var1, CipherParameters var2);

	int processBytes(byte[] var1, int var2, int var3, byte[] var4, int var5);

	void reset();

	byte returnByte(byte var1);
}
