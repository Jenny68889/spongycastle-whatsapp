package org.spongycastle.crypto.paddings;

import java.security.SecureRandom;

import org.spongycastle.crypto.CryptoException;

public interface BlockCipherPadding {
	int addPadding(byte[] var1, int var2);

	String getPaddingName();

	void init(SecureRandom var1);

	int padCount(byte[] var1) throws CryptoException;
}
