package org.spongycastle.crypto.paddings;

import java.security.SecureRandom;

import org.spongycastle.crypto.CryptoException;
import org.spongycastle.crypto.InvalidCipherTextException;

public class PKCS7Padding implements BlockCipherPadding {
	public int addPadding(byte[] var1, int var2) {
		byte var3;
		for (var3 = (byte) (var1.length - var2); var2 < var1.length; ++var2) {
			var1[var2] = var3;
		}

		return var3;
	}

	public String getPaddingName() {
		return "PKCS7";
	}

	public void init(SecureRandom var1) {
	}

	public int padCount(byte[] var1) throws CryptoException {
		int var2 = 255 & var1[-1 + var1.length];
		byte var3 = (byte) var2;
		boolean var4;
		if (var2 > var1.length) {
			var4 = true;
		} else {
			var4 = false;
		}

		boolean var5;
		if (var2 == 0) {
			var5 = true;
		} else {
			var5 = false;
		}

		boolean var6 = var5 | var4;

		for (int var7 = 0; var7 < var1.length; ++var7) {
			boolean var8;
			if (var1.length - var7 <= var2) {
				var8 = true;
			} else {
				var8 = false;
			}

			boolean var9;
			if (var1[var7] != var3) {
				var9 = true;
			} else {
				var9 = false;
			}

			var6 |= var8 & var9;
		}

		if (var6) {
			throw new InvalidCipherTextException("pad block corrupted");
		} else {
			return var2;
		}
	}
}
