package org.spongycastle.crypto;

public abstract class StreamBlockCipher implements BlockCipher, StreamCipher {
	private final BlockCipher cipher;

	protected StreamBlockCipher(BlockCipher var1) {
		this.cipher = var1;
	}

	protected abstract byte calculateByte(byte var1);

	public BlockCipher getUnderlyingCipher() {
		return this.cipher;
	}

	public int processBytes(byte[] var1, int var2, int var3, byte[] var4, int var5) {
		if (var5 + var3 > var4.length) {
			throw new DataLengthException("output buffer too short");
		} else if (var2 + var3 > var1.length) {
			throw new DataLengthException("input buffer too small");
		} else {
			int var8;
			for (int var6 = var2 + var3; var2 < var6; var2 = var8) {
				int var7 = var5 + 1;
				var8 = var2 + 1;
				var4[var5] = this.calculateByte(var1[var2]);
				var5 = var7;
			}

			return var3;
		}
	}

	public final byte returnByte(byte var1) {
		return this.calculateByte(var1);
	}
}
