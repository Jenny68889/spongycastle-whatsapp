package org.spongycastle.crypto;

import org.spongycastle.util.Strings;

public abstract class PBEParametersGenerator {
	protected int iterationCount;
	protected byte[] password;
	protected byte[] salt;

	protected PBEParametersGenerator() {
	}

	public static byte[] PKCS12PasswordToBytes(char[] var0) {
		int var1 = 0;
		if (var0 != null && var0.length > 0) {
			byte[] var2;
			for (var2 = new byte[2 * (1 + var0.length)]; var1 != var0.length; ++var1) {
				var2[var1 * 2] = (byte) (var0[var1] >>> 8);
				var2[1 + var1 * 2] = (byte) var0[var1];
			}

			return var2;
		} else {
			return new byte[0];
		}
	}

	public static byte[] PKCS5PasswordToBytes(char[] var0) {
		int var1 = 0;
		if (var0 == null) {
			return new byte[0];
		} else {
			byte[] var2;
			for (var2 = new byte[var0.length]; var1 != var2.length; ++var1) {
				var2[var1] = (byte) var0[var1];
			}

			return var2;
		}
	}

	public static byte[] PKCS5PasswordToUTF8Bytes(char[] var0) {
		return var0 != null ? Strings.toUTF8ByteArray(var0) : new byte[0];
	}

	public abstract CipherParameters generateDerivedMacParameters(int var1);

	public abstract CipherParameters generateDerivedParameters(int var1);

	public abstract CipherParameters generateDerivedParameters(int var1, int var2);

	public int getIterationCount() {
		return this.iterationCount;
	}

	public byte[] getPassword() {
		return this.password;
	}

	public byte[] getSalt() {
		return this.salt;
	}

	public void init(byte[] var1, byte[] var2, int var3) {
		this.password = var1;
		this.salt = var2;
		this.iterationCount = var3;
	}
}
