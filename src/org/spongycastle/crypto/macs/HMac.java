package org.spongycastle.crypto.macs;

import java.util.Hashtable;
import org.spongycastle.crypto.CipherParameters;
import org.spongycastle.crypto.Digest;
import org.spongycastle.crypto.ExtendedDigest;
import org.spongycastle.crypto.Mac;
import org.spongycastle.crypto.params.KeyParameter;
import org.spongycastle.util.Memoable;

public class HMac implements Mac {
	private static final byte IPAD = 54;
	private static final byte OPAD = 92;
	private static Hashtable blockLengths = new Hashtable();
	private int blockLength;
	private Digest digest;
	private int digestSize;
	private byte[] inputPad;
	private Memoable ipadState;
	private Memoable opadState;
	private byte[] outputBuf;

	public HMac(Digest var1) {
		this(var1, getByteLength(var1));
	}

	private HMac(Digest var1, int var2) {
		this.digest = var1;
		this.digestSize = var1.getDigestSize();
		this.blockLength = var2;
		this.inputPad = new byte[this.blockLength];
		this.outputBuf = new byte[this.blockLength + this.digestSize];
	}

	private static int getByteLength(Digest var0) {
		if (var0 instanceof ExtendedDigest) {
			return ((ExtendedDigest) var0).getByteLength();
		} else {
			Integer var1 = (Integer) blockLengths.get(var0.getAlgorithmName());
			if (var1 == null) {
				throw new IllegalArgumentException("unknown digest passed: " + var0.getAlgorithmName());
			} else {
				return var1;
			}
		}
	}

	private static void xorPad(byte[] var0, int var1, byte var2) {
		for (int var3 = 0; var3 < var1; ++var3) {
			var0[var3] ^= var2;
		}

	}

	public int doFinal(byte[] var1, int var2) {
		this.digest.doFinal(this.outputBuf, this.blockLength);
		if (this.opadState != null) {
			((Memoable) this.digest).reset(this.opadState);
			this.digest.update(this.outputBuf, this.blockLength, this.digest.getDigestSize());
		} else {
			this.digest.update(this.outputBuf, 0, this.outputBuf.length);
		}

		int var4 = this.digest.doFinal(var1, var2);

		for (int var5 = this.blockLength; var5 < this.outputBuf.length; ++var5) {
			this.outputBuf[var5] = 0;
		}

		if (this.ipadState != null) {
			((Memoable) this.digest).reset(this.ipadState);
			return var4;
		} else {
			this.digest.update(this.inputPad, 0, this.inputPad.length);
			return var4;
		}
	}

	public String getAlgorithmName() {
		return this.digest.getAlgorithmName() + "/HMAC";
	}

	public int getMacSize() {
		return this.digestSize;
	}

	public Digest getUnderlyingDigest() {
		return this.digest;
	}

	public void init(CipherParameters var1) {
		this.digest.reset();
		byte[] var2 = ((KeyParameter) var1).getKey();
		int var3 = var2.length;
		if (var3 > this.blockLength) {
			this.digest.update(var2, 0, var3);
			this.digest.doFinal(this.inputPad, 0);
			var3 = this.digestSize;
		} else {
			System.arraycopy(var2, 0, this.inputPad, 0, var3);
		}

		while (var3 < this.inputPad.length) {
			this.inputPad[var3] = 0;
			++var3;
		}

		System.arraycopy(this.inputPad, 0, this.outputBuf, 0, this.blockLength);
		xorPad(this.inputPad, this.blockLength, (byte) 54);
		xorPad(this.outputBuf, this.blockLength, (byte) 92);
		if (this.digest instanceof Memoable) {
			this.opadState = ((Memoable) this.digest).copy();
			((Digest) this.opadState).update(this.outputBuf, 0, this.blockLength);
		}

		this.digest.update(this.inputPad, 0, this.inputPad.length);
		if (this.digest instanceof Memoable) {
			this.ipadState = ((Memoable) this.digest).copy();
		}

	}

	public void reset() {
		this.digest.reset();
		this.digest.update(this.inputPad, 0, this.inputPad.length);
	}

	public void update(byte var1) {
		this.digest.update(var1);
	}

	public void update(byte[] var1, int var2, int var3) {
		this.digest.update(var1, var2, var3);
	}
}
