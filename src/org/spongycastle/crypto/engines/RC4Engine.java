package org.spongycastle.crypto.engines;

import org.spongycastle.crypto.CipherParameters;
import org.spongycastle.crypto.DataLengthException;
import org.spongycastle.crypto.OutputLengthException;
import org.spongycastle.crypto.StreamCipher;
import org.spongycastle.crypto.params.KeyParameter;

public class RC4Engine implements StreamCipher {
	private static final int STATE_LENGTH = 256;
	private byte[] engineState = null;
	private byte[] workingKey = null;
	private int x = 0;
	private int y = 0;

	private void setKey(byte[] var1) {
		int var2 = 0;
		this.workingKey = var1;
		this.x = 0;
		this.y = 0;
		if (this.engineState == null) {
			this.engineState = new byte[256];
		}

		for (int var3 = 0; var3 < 256; ++var3) {
			this.engineState[var3] = (byte) var3;
		}

		int var4 = 0;

		for (int var5 = 0; var2 < 256; ++var2) {
			var4 = 255 & var4 + (255 & var1[var5]) + this.engineState[var2];
			byte var6 = this.engineState[var2];
			this.engineState[var2] = this.engineState[var4];
			this.engineState[var4] = var6;
			var5 = (var5 + 1) % var1.length;
		}

	}

	public String getAlgorithmName() {
		return "RC4";
	}

	public void init(boolean var1, CipherParameters var2) {
		if (var2 instanceof KeyParameter) {
			this.workingKey = ((KeyParameter) var2).getKey();
			this.setKey(this.workingKey);
		} else {
			throw new IllegalArgumentException("invalid parameter passed to RC4 init - " + var2.getClass().getName());
		}
	}

	public int processBytes(byte[] var1, int var2, int var3, byte[] var4, int var5) {
		if (var2 + var3 > var1.length) {
			throw new DataLengthException("input buffer too short");
		} else if (var5 + var3 > var4.length) {
			throw new OutputLengthException("output buffer too short");
		} else {
			for (int var6 = 0; var6 < var3; ++var6) {
				this.x = 255 & 1 + this.x;
				this.y = 255 & this.engineState[this.x] + this.y;
				byte var7 = this.engineState[this.x];
				this.engineState[this.x] = this.engineState[this.y];
				this.engineState[this.y] = var7;
				var4[var6 + var5] = (byte) (var1[var6 + var2]
						^ this.engineState[255 & this.engineState[this.x] + this.engineState[this.y]]);
			}

			return var3;
		}
	}

	public void reset() {
		this.setKey(this.workingKey);
	}

	public byte returnByte(byte var1) {
		this.x = 255 & 1 + this.x;
		this.y = 255 & this.engineState[this.x] + this.y;
		byte var2 = this.engineState[this.x];
		this.engineState[this.x] = this.engineState[this.y];
		this.engineState[this.y] = var2;
		return (byte) (var1 ^ this.engineState[255 & this.engineState[this.x] + this.engineState[this.y]]);
	}
}
