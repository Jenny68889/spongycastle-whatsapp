package org.spongycastle.crypto.digests;

import org.spongycastle.util.Memoable;
import org.spongycastle.util.Pack;

public class SHA224Digest extends GeneralDigest implements EncodableDigest {
	private static final int DIGEST_LENGTH = 28;
	static final int[] K = new int[] { 1116352408, 1899447441, -1245643825, -373957723, 961987163, 1508970993,
			-1841331548, -1424204075, -670586216, 310598401, 607225278, 1426881987, 1925078388, -2132889090,
			-1680079193, -1046744716, -459576895, -272742522, 264347078, 604807628, 770255983, 1249150122, 1555081692,
			1996064986, -1740746414, -1473132947, -1341970488, -1084653625, -958395405, -710438585, 113926993,
			338241895, 666307205, 773529912, 1294757372, 1396182291, 1695183700, 1986661051, -2117940946, -1838011259,
			-1564481375, -1474664885, -1035236496, -949202525, -778901479, -694614492, -200395387, 275423344, 430227734,
			506948616, 659060556, 883997877, 958139571, 1322822218, 1537002063, 1747873779, 1955562222, 2024104815,
			-2067236844, -1933114872, -1866530822, -1538233109, -1090935817, -965641998 };
	private int H1;
	private int H2;
	private int H3;
	private int H4;
	private int H5;
	private int H6;
	private int H7;
	private int H8;
	private int[] X = new int[64];
	private int xOff;

	public SHA224Digest() {
		this.reset();
	}

	public SHA224Digest(SHA224Digest var1) {
		super((GeneralDigest) var1);
		this.doCopy(var1);
	}

	public SHA224Digest(byte[] var1) {
		super(var1);
		this.H1 = Pack.bigEndianToInt(var1, 16);
		this.H2 = Pack.bigEndianToInt(var1, 20);
		this.H3 = Pack.bigEndianToInt(var1, 24);
		this.H4 = Pack.bigEndianToInt(var1, 28);
		this.H5 = Pack.bigEndianToInt(var1, 32);
		this.H6 = Pack.bigEndianToInt(var1, 36);
		this.H7 = Pack.bigEndianToInt(var1, 40);
		this.H8 = Pack.bigEndianToInt(var1, 44);
		this.xOff = Pack.bigEndianToInt(var1, 48);

		for (int var2 = 0; var2 != this.xOff; ++var2) {
			this.X[var2] = Pack.bigEndianToInt(var1, 52 + var2 * 4);
		}

	}

	private int Ch(int var1, int var2, int var3) {
		return var1 & var2 ^ var3 & ~var1;
	}

	private int Maj(int var1, int var2, int var3) {
		return var1 & var2 ^ var1 & var3 ^ var2 & var3;
	}

	private int Sum0(int var1) {
		return (var1 >>> 2 | var1 << 30) ^ (var1 >>> 13 | var1 << 19) ^ (var1 >>> 22 | var1 << 10);
	}

	private int Sum1(int var1) {
		return (var1 >>> 6 | var1 << 26) ^ (var1 >>> 11 | var1 << 21) ^ (var1 >>> 25 | var1 << 7);
	}

	private int Theta0(int var1) {
		return (var1 >>> 7 | var1 << 25) ^ (var1 >>> 18 | var1 << 14) ^ var1 >>> 3;
	}

	private int Theta1(int var1) {
		return (var1 >>> 17 | var1 << 15) ^ (var1 >>> 19 | var1 << 13) ^ var1 >>> 10;
	}

	private void doCopy(SHA224Digest var1) {
		super.copyIn(var1);
		this.H1 = var1.H1;
		this.H2 = var1.H2;
		this.H3 = var1.H3;
		this.H4 = var1.H4;
		this.H5 = var1.H5;
		this.H6 = var1.H6;
		this.H7 = var1.H7;
		this.H8 = var1.H8;
		System.arraycopy(var1.X, 0, this.X, 0, var1.X.length);
		this.xOff = var1.xOff;
	}

	public Memoable copy() {
		return new SHA224Digest(this);
	}

	public int doFinal(byte[] var1, int var2) {
		this.finish();
		Pack.intToBigEndian(this.H1, var1, var2);
		Pack.intToBigEndian(this.H2, var1, var2 + 4);
		Pack.intToBigEndian(this.H3, var1, var2 + 8);
		Pack.intToBigEndian(this.H4, var1, var2 + 12);
		Pack.intToBigEndian(this.H5, var1, var2 + 16);
		Pack.intToBigEndian(this.H6, var1, var2 + 20);
		Pack.intToBigEndian(this.H7, var1, var2 + 24);
		this.reset();
		return 28;
	}

	public String getAlgorithmName() {
		return "SHA-224";
	}

	public int getDigestSize() {
		return 28;
	}

	public byte[] getEncodedState() {
		byte[] var1 = new byte[52 + 4 * this.xOff];
		super.populateState(var1);
		Pack.intToBigEndian(this.H1, var1, 16);
		Pack.intToBigEndian(this.H2, var1, 20);
		Pack.intToBigEndian(this.H3, var1, 24);
		Pack.intToBigEndian(this.H4, var1, 28);
		Pack.intToBigEndian(this.H5, var1, 32);
		Pack.intToBigEndian(this.H6, var1, 36);
		Pack.intToBigEndian(this.H7, var1, 40);
		Pack.intToBigEndian(this.H8, var1, 44);
		Pack.intToBigEndian(this.xOff, var1, 48);

		for (int var2 = 0; var2 != this.xOff; ++var2) {
			Pack.intToBigEndian(this.X[var2], var1, 52 + var2 * 4);
		}

		return var1;
	}

	protected void processBlock() {
		for (int var1 = 16; var1 <= 63; ++var1) {
			this.X[var1] = this.Theta1(this.X[var1 - 2]) + this.X[var1 - 7] + this.Theta0(this.X[var1 - 15])
					+ this.X[var1 - 16];
		}

		int var2 = this.H1;
		int var3 = this.H2;
		int var4 = this.H3;
		int var5 = this.H4;
		int var6 = this.H5;
		int var7 = this.H6;
		int var8 = this.H7;
		int var9 = this.H8;
		int var10 = var3;
		int var11 = var2;
		int var12 = var5;
		int var13 = var4;
		int var14 = var7;
		int var15 = var6;
		int var16 = var9;
		int var17 = var8;
		int var18 = 0;

		for (int var19 = 0; var18 < 8; ++var18) {
			int var20 = var16 + this.Sum1(var15) + this.Ch(var15, var14, var17) + K[var19] + this.X[var19];
			int var21 = var12 + var20;
			int var22 = var20 + this.Sum0(var11) + this.Maj(var11, var10, var13);
			int var23 = var19 + 1;
			int var24 = var17 + this.Sum1(var21) + this.Ch(var21, var15, var14) + K[var23] + this.X[var23];
			int var25 = var13 + var24;
			int var26 = var24 + this.Sum0(var22) + this.Maj(var22, var11, var10);
			int var27 = var23 + 1;
			int var28 = var14 + this.Sum1(var25) + this.Ch(var25, var21, var15) + K[var27] + this.X[var27];
			int var29 = var10 + var28;
			int var30 = var28 + this.Sum0(var26) + this.Maj(var26, var22, var11);
			int var31 = var27 + 1;
			int var32 = var15 + this.Sum1(var29) + this.Ch(var29, var25, var21) + K[var31] + this.X[var31];
			int var33 = var11 + var32;
			int var34 = var32 + this.Sum0(var30) + this.Maj(var30, var26, var22);
			int var35 = var31 + 1;
			int var36 = var21 + this.Sum1(var33) + this.Ch(var33, var29, var25) + K[var35] + this.X[var35];
			var16 = var22 + var36;
			var12 = var36 + this.Sum0(var34) + this.Maj(var34, var30, var26);
			int var37 = var35 + 1;
			int var38 = var25 + this.Sum1(var16) + this.Ch(var16, var33, var29) + K[var37] + this.X[var37];
			var17 = var26 + var38;
			var13 = var38 + this.Sum0(var12) + this.Maj(var12, var34, var30);
			int var39 = var37 + 1;
			int var40 = var29 + this.Sum1(var17) + this.Ch(var17, var16, var33) + K[var39] + this.X[var39];
			var14 = var30 + var40;
			var10 = var40 + this.Sum0(var13) + this.Maj(var13, var12, var34);
			int var41 = var39 + 1;
			int var42 = var33 + this.Sum1(var14) + this.Ch(var14, var17, var16) + K[var41] + this.X[var41];
			var15 = var34 + var42;
			var11 = var42 + this.Sum0(var10) + this.Maj(var10, var13, var12);
			var19 = var41 + 1;
		}

		this.H1 += var11;
		this.H2 += var10;
		this.H3 += var13;
		this.H4 += var12;
		this.H5 += var15;
		this.H6 += var14;
		this.H7 += var17;
		this.H8 += var16;
		this.xOff = 0;

		for (int var43 = 0; var43 < 16; ++var43) {
			this.X[var43] = 0;
		}

	}

	protected void processLength(long var1) {
		if (this.xOff > 14) {
			this.processBlock();
		}

		this.X[14] = (int) (var1 >>> 32);
		this.X[15] = (int) (-1L & var1);
	}

	protected void processWord(byte[] var1, int var2) {
		int var3 = var1[var2] << 24;
		int var4 = var2 + 1;
		int var5 = var3 | (255 & var1[var4]) << 16;
		int var6 = var4 + 1;
		int var7 = var5 | (255 & var1[var6]) << 8 | 255 & var1[var6 + 1];
		this.X[this.xOff] = var7;
		int var8 = 1 + this.xOff;
		this.xOff = var8;
		if (var8 == 16) {
			this.processBlock();
		}

	}

	public void reset() {
		super.reset();
		this.H1 = -1056596264;
		this.H2 = 914150663;
		this.H3 = 812702999;
		this.H4 = -150054599;
		this.H5 = -4191439;
		this.H6 = 1750603025;
		this.H7 = 1694076839;
		this.H8 = -1090891868;
		this.xOff = 0;

		for (int var1 = 0; var1 != this.X.length; ++var1) {
			this.X[var1] = 0;
		}

	}

	public void reset(Memoable var1) {
		this.doCopy((SHA224Digest) var1);
	}
}
