package org.spongycastle.crypto.digests;

import org.spongycastle.util.Memoable;

public class MD5Digest extends GeneralDigest {
	private static final int DIGEST_LENGTH = 16;
	private static final int S11 = 7;
	private static final int S12 = 12;
	private static final int S13 = 17;
	private static final int S14 = 22;
	private static final int S21 = 5;
	private static final int S22 = 9;
	private static final int S23 = 14;
	private static final int S24 = 20;
	private static final int S31 = 4;
	private static final int S32 = 11;
	private static final int S33 = 16;
	private static final int S34 = 23;
	private static final int S41 = 6;
	private static final int S42 = 10;
	private static final int S43 = 15;
	private static final int S44 = 21;
	private int H1;
	private int H2;
	private int H3;
	private int H4;
	private int[] X = new int[16];
	private int xOff;

	public MD5Digest() {
		this.reset();
	}

	public MD5Digest(MD5Digest var1) {
		super((GeneralDigest) var1);
		this.copyIn(var1);
	}

	private int F(int var1, int var2, int var3) {
		return var1 & var2 | var3 & ~var1;
	}

	private int G(int var1, int var2, int var3) {
		return var1 & var3 | var2 & ~var3;
	}

	private int H(int var1, int var2, int var3) {
		return var3 ^ var1 ^ var2;
	}

	private int K(int var1, int var2, int var3) {
		return var2 ^ (var1 | ~var3);
	}

	private void copyIn(MD5Digest var1) {
		super.copyIn(var1);
		this.H1 = var1.H1;
		this.H2 = var1.H2;
		this.H3 = var1.H3;
		this.H4 = var1.H4;
		System.arraycopy(var1.X, 0, this.X, 0, var1.X.length);
		this.xOff = var1.xOff;
	}

	private int rotateLeft(int var1, int var2) {
		return var1 << var2 | var1 >>> 32 - var2;
	}

	private void unpackWord(int var1, byte[] var2, int var3) {
		var2[var3] = (byte) var1;
		var2[var3 + 1] = (byte) (var1 >>> 8);
		var2[var3 + 2] = (byte) (var1 >>> 16);
		var2[var3 + 3] = (byte) (var1 >>> 24);
	}

	public Memoable copy() {
		return new MD5Digest(this);
	}

	public int doFinal(byte[] var1, int var2) {
		this.finish();
		this.unpackWord(this.H1, var1, var2);
		this.unpackWord(this.H2, var1, var2 + 4);
		this.unpackWord(this.H3, var1, var2 + 8);
		this.unpackWord(this.H4, var1, var2 + 12);
		this.reset();
		return 16;
	}

	public String getAlgorithmName() {
		return "MD5";
	}

	public int getDigestSize() {
		return 16;
	}

	protected void processBlock() {
		int var1 = this.H1;
		int var2 = this.H2;
		int var3 = this.H3;
		int var4 = this.H4;
		int var5 = var2 + this.rotateLeft(-680876936 + var1 + this.F(var2, var3, var4) + this.X[0], 7);
		int var6 = var5 + this.rotateLeft(-389564586 + var4 + this.F(var5, var2, var3) + this.X[1], 12);
		int var7 = var6 + this.rotateLeft(606105819 + var3 + this.F(var6, var5, var2) + this.X[2], 17);
		int var8 = var7 + this.rotateLeft(-1044525330 + var2 + this.F(var7, var6, var5) + this.X[3], 22);
		int var9 = var8 + this.rotateLeft(-176418897 + var5 + this.F(var8, var7, var6) + this.X[4], 7);
		int var10 = var9 + this.rotateLeft(1200080426 + var6 + this.F(var9, var8, var7) + this.X[5], 12);
		int var11 = var10 + this.rotateLeft(-1473231341 + var7 + this.F(var10, var9, var8) + this.X[6], 17);
		int var12 = var11 + this.rotateLeft(-45705983 + var8 + this.F(var11, var10, var9) + this.X[7], 22);
		int var13 = var12 + this.rotateLeft(1770035416 + var9 + this.F(var12, var11, var10) + this.X[8], 7);
		int var14 = var13 + this.rotateLeft(-1958414417 + var10 + this.F(var13, var12, var11) + this.X[9], 12);
		int var15 = var14 + this.rotateLeft(-42063 + var11 + this.F(var14, var13, var12) + this.X[10], 17);
		int var16 = var15 + this.rotateLeft(-1990404162 + var12 + this.F(var15, var14, var13) + this.X[11], 22);
		int var17 = var16 + this.rotateLeft(1804603682 + var13 + this.F(var16, var15, var14) + this.X[12], 7);
		int var18 = var17 + this.rotateLeft(-40341101 + var14 + this.F(var17, var16, var15) + this.X[13], 12);
		int var19 = var18 + this.rotateLeft(-1502002290 + var15 + this.F(var18, var17, var16) + this.X[14], 17);
		int var20 = var19 + this.rotateLeft(1236535329 + var16 + this.F(var19, var18, var17) + this.X[15], 22);
		int var21 = var20 + this.rotateLeft(-165796510 + var17 + this.G(var20, var19, var18) + this.X[1], 5);
		int var22 = var21 + this.rotateLeft(-1069501632 + var18 + this.G(var21, var20, var19) + this.X[6], 9);
		int var23 = var22 + this.rotateLeft(643717713 + var19 + this.G(var22, var21, var20) + this.X[11], 14);
		int var24 = var23 + this.rotateLeft(-373897302 + var20 + this.G(var23, var22, var21) + this.X[0], 20);
		int var25 = var24 + this.rotateLeft(-701558691 + var21 + this.G(var24, var23, var22) + this.X[5], 5);
		int var26 = var25 + this.rotateLeft(38016083 + var22 + this.G(var25, var24, var23) + this.X[10], 9);
		int var27 = var26 + this.rotateLeft(-660478335 + var23 + this.G(var26, var25, var24) + this.X[15], 14);
		int var28 = var27 + this.rotateLeft(-405537848 + var24 + this.G(var27, var26, var25) + this.X[4], 20);
		int var29 = var28 + this.rotateLeft(568446438 + var25 + this.G(var28, var27, var26) + this.X[9], 5);
		int var30 = var29 + this.rotateLeft(-1019803690 + var26 + this.G(var29, var28, var27) + this.X[14], 9);
		int var31 = var30 + this.rotateLeft(-187363961 + var27 + this.G(var30, var29, var28) + this.X[3], 14);
		int var32 = var31 + this.rotateLeft(1163531501 + var28 + this.G(var31, var30, var29) + this.X[8], 20);
		int var33 = var32 + this.rotateLeft(-1444681467 + var29 + this.G(var32, var31, var30) + this.X[13], 5);
		int var34 = var33 + this.rotateLeft(-51403784 + var30 + this.G(var33, var32, var31) + this.X[2], 9);
		int var35 = var34 + this.rotateLeft(1735328473 + var31 + this.G(var34, var33, var32) + this.X[7], 14);
		int var36 = var35 + this.rotateLeft(-1926607734 + var32 + this.G(var35, var34, var33) + this.X[12], 20);
		int var37 = var36 + this.rotateLeft(-378558 + var33 + this.H(var36, var35, var34) + this.X[5], 4);
		int var38 = var37 + this.rotateLeft(-2022574463 + var34 + this.H(var37, var36, var35) + this.X[8], 11);
		int var39 = var38 + this.rotateLeft(1839030562 + var35 + this.H(var38, var37, var36) + this.X[11], 16);
		int var40 = var39 + this.rotateLeft(-35309556 + var36 + this.H(var39, var38, var37) + this.X[14], 23);
		int var41 = var40 + this.rotateLeft(-1530992060 + var37 + this.H(var40, var39, var38) + this.X[1], 4);
		int var42 = var41 + this.rotateLeft(1272893353 + var38 + this.H(var41, var40, var39) + this.X[4], 11);
		int var43 = var42 + this.rotateLeft(-155497632 + var39 + this.H(var42, var41, var40) + this.X[7], 16);
		int var44 = var43 + this.rotateLeft(-1094730640 + var40 + this.H(var43, var42, var41) + this.X[10], 23);
		int var45 = var44 + this.rotateLeft(681279174 + var41 + this.H(var44, var43, var42) + this.X[13], 4);
		int var46 = var45 + this.rotateLeft(-358537222 + var42 + this.H(var45, var44, var43) + this.X[0], 11);
		int var47 = var46 + this.rotateLeft(-722521979 + var43 + this.H(var46, var45, var44) + this.X[3], 16);
		int var48 = var47 + this.rotateLeft(76029189 + var44 + this.H(var47, var46, var45) + this.X[6], 23);
		int var49 = var48 + this.rotateLeft(-640364487 + var45 + this.H(var48, var47, var46) + this.X[9], 4);
		int var50 = var49 + this.rotateLeft(-421815835 + var46 + this.H(var49, var48, var47) + this.X[12], 11);
		int var51 = var50 + this.rotateLeft(530742520 + var47 + this.H(var50, var49, var48) + this.X[15], 16);
		int var52 = var51 + this.rotateLeft(-995338651 + var48 + this.H(var51, var50, var49) + this.X[2], 23);
		int var53 = var52 + this.rotateLeft(-198630844 + var49 + this.K(var52, var51, var50) + this.X[0], 6);
		int var54 = var53 + this.rotateLeft(1126891415 + var50 + this.K(var53, var52, var51) + this.X[7], 10);
		int var55 = var54 + this.rotateLeft(-1416354905 + var51 + this.K(var54, var53, var52) + this.X[14], 15);
		int var56 = var55 + this.rotateLeft(-57434055 + var52 + this.K(var55, var54, var53) + this.X[5], 21);
		int var57 = var56 + this.rotateLeft(1700485571 + var53 + this.K(var56, var55, var54) + this.X[12], 6);
		int var58 = var57 + this.rotateLeft(-1894986606 + var54 + this.K(var57, var56, var55) + this.X[3], 10);
		int var59 = var58 + this.rotateLeft(-1051523 + var55 + this.K(var58, var57, var56) + this.X[10], 15);
		int var60 = var59 + this.rotateLeft(-2054922799 + var56 + this.K(var59, var58, var57) + this.X[1], 21);
		int var61 = var60 + this.rotateLeft(1873313359 + var57 + this.K(var60, var59, var58) + this.X[8], 6);
		int var62 = var61 + this.rotateLeft(-30611744 + var58 + this.K(var61, var60, var59) + this.X[15], 10);
		int var63 = var62 + this.rotateLeft(-1560198380 + var59 + this.K(var62, var61, var60) + this.X[6], 15);
		int var64 = var63 + this.rotateLeft(1309151649 + var60 + this.K(var63, var62, var61) + this.X[13], 21);
		int var65 = var64 + this.rotateLeft(-145523070 + var61 + this.K(var64, var63, var62) + this.X[4], 6);
		int var66 = var65 + this.rotateLeft(-1120210379 + var62 + this.K(var65, var64, var63) + this.X[11], 10);
		int var67 = var66 + this.rotateLeft(718787259 + var63 + this.K(var66, var65, var64) + this.X[2], 15);
		int var68 = var67 + this.rotateLeft(-343485551 + var64 + this.K(var67, var66, var65) + this.X[9], 21);
		this.H1 += var65;
		this.H2 += var68;
		this.H3 += var67;
		this.H4 += var66;
		this.xOff = 0;

		for (int var69 = 0; var69 != this.X.length; ++var69) {
			this.X[var69] = 0;
		}

	}

	protected void processLength(long var1) {
		if (this.xOff > 14) {
			this.processBlock();
		}

		this.X[14] = (int) (-1L & var1);
		this.X[15] = (int) (var1 >>> 32);
	}

	protected void processWord(byte[] var1, int var2) {
		int[] var3 = this.X;
		int var4 = this.xOff++;
		var3[var4] = 255 & var1[var2] | (255 & var1[var2 + 1]) << 8 | (255 & var1[var2 + 2]) << 16
				| (255 & var1[var2 + 3]) << 24;
		if (this.xOff == 16) {
			this.processBlock();
		}

	}

	public void reset() {
		super.reset();
		this.H1 = 1732584193;
		this.H2 = -271733879;
		this.H3 = -1732584194;
		this.H4 = 271733878;
		this.xOff = 0;

		for (int var1 = 0; var1 != this.X.length; ++var1) {
			this.X[var1] = 0;
		}

	}

	public void reset(Memoable var1) {
		this.copyIn((MD5Digest) var1);
	}
}
