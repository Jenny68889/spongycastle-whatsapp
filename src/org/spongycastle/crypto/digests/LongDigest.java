package org.spongycastle.crypto.digests;

import org.spongycastle.crypto.ExtendedDigest;
import org.spongycastle.util.Memoable;
import org.spongycastle.util.Pack;

public abstract class LongDigest implements ExtendedDigest, EncodableDigest, Memoable {
	private static final int BYTE_LENGTH = 128;
	static final long[] K = new long[] { 4794697086780616226L, 8158064640168781261L, -5349999486874862801L,
			-1606136188198331460L, 4131703408338449720L, 6480981068601479193L, -7908458776815382629L,
			-6116909921290321640L, -2880145864133508542L, 1334009975649890238L, 2608012711638119052L,
			6128411473006802146L, 8268148722764581231L, -9160688886553864527L, -7215885187991268811L,
			-4495734319001033068L, -1973867731355612462L, -1171420211273849373L, 1135362057144423861L,
			2597628984639134821L, 3308224258029322869L, 5365058923640841347L, 6679025012923562964L,
			8573033837759648693L, -7476448914759557205L, -6327057829258317296L, -5763719355590565569L,
			-4658551843659510044L, -4116276920077217854L, -3051310485924567259L, 489312712824947311L,
			1452737877330783856L, 2861767655752347644L, 3322285676063803686L, 5560940570517711597L,
			5996557281743188959L, 7280758554555802590L, 8532644243296465576L, -9096487096722542874L,
			-7894198246740708037L, -6719396339535248540L, -6333637450476146687L, -4446306890439682159L,
			-4076793802049405392L, -3345356375505022440L, -2983346525034927856L, -860691631967231958L,
			1182934255886127544L, 1847814050463011016L, 2177327727835720531L, 2830643537854262169L,
			3796741975233480872L, 4115178125766777443L, 5681478168544905931L, 6601373596472566643L,
			7507060721942968483L, 8399075790359081724L, 8693463985226723168L, -8878714635349349518L,
			-8302665154208450068L, -8016688836872298968L, -6606660893046293015L, -4685533653050689259L,
			-4147400797238176981L, -3880063495543823972L, -3348786107499101689L, -1523767162380948706L,
			-757361751448694408L, 500013540394364858L, 748580250866718886L, 1242879168328830382L, 1977374033974150939L,
			2944078676154940804L, 3659926193048069267L, 4368137639120453308L, 4836135668995329356L,
			5532061633213252278L, 6448918945643986474L, 6902733635092675308L, 7801388544844847127L };
	protected long H1;
	protected long H2;
	protected long H3;
	protected long H4;
	protected long H5;
	protected long H6;
	protected long H7;
	protected long H8;
	private long[] W = new long[80];
	private long byteCount1;
	private long byteCount2;
	private int wOff;
	private byte[] xBuf = new byte[8];
	private int xBufOff;

	protected LongDigest() {
		this.xBufOff = 0;
		this.reset();
	}

	protected LongDigest(LongDigest var1) {
		this.copyIn(var1);
	}

	private long Ch(long var1, long var3, long var5) {
		return var1 & var3 ^ var5 & ~var1;
	}

	private long Maj(long var1, long var3, long var5) {
		return var1 & var3 ^ var1 & var5 ^ var3 & var5;
	}

	private long Sigma0(long var1) {
		return (var1 << 63 | var1 >>> 1) ^ (var1 << 56 | var1 >>> 8) ^ var1 >>> 7;
	}

	private long Sigma1(long var1) {
		return (var1 << 45 | var1 >>> 19) ^ (var1 << 3 | var1 >>> 61) ^ var1 >>> 6;
	}

	private long Sum0(long var1) {
		return (var1 << 36 | var1 >>> 28) ^ (var1 << 30 | var1 >>> 34) ^ (var1 << 25 | var1 >>> 39);
	}

	private long Sum1(long var1) {
		return (var1 << 50 | var1 >>> 14) ^ (var1 << 46 | var1 >>> 18) ^ (var1 << 23 | var1 >>> 41);
	}

	private void adjustByteCounts() {
		if (this.byteCount1 > 2305843009213693951L) {
			this.byteCount2 += this.byteCount1 >>> 61;
			this.byteCount1 &= 2305843009213693951L;
		}

	}

	protected void copyIn(LongDigest var1) {
		System.arraycopy(var1.xBuf, 0, this.xBuf, 0, var1.xBuf.length);
		this.xBufOff = var1.xBufOff;
		this.byteCount1 = var1.byteCount1;
		this.byteCount2 = var1.byteCount2;
		this.H1 = var1.H1;
		this.H2 = var1.H2;
		this.H3 = var1.H3;
		this.H4 = var1.H4;
		this.H5 = var1.H5;
		this.H6 = var1.H6;
		this.H7 = var1.H7;
		this.H8 = var1.H8;
		System.arraycopy(var1.W, 0, this.W, 0, var1.W.length);
		this.wOff = var1.wOff;
	}

	public void finish() {
		this.adjustByteCounts();
		long var1 = this.byteCount1 << 3;
		long var3 = this.byteCount2;
		this.update((byte) -128);

		while (this.xBufOff != 0) {
			this.update((byte) 0);
		}

		this.processLength(var1, var3);
		this.processBlock();
	}

	public int getByteLength() {
		return 128;
	}

	protected int getEncodedStateSize() {
		return 96 + 8 * this.wOff;
	}

	protected void populateState(byte[] var1) {
		int var2 = 0;
		System.arraycopy(this.xBuf, 0, var1, 0, this.xBufOff);
		Pack.intToBigEndian(this.xBufOff, var1, 8);
		Pack.longToBigEndian(this.byteCount1, var1, 12);
		Pack.longToBigEndian(this.byteCount2, var1, 20);
		Pack.longToBigEndian(this.H1, var1, 28);
		Pack.longToBigEndian(this.H2, var1, 36);
		Pack.longToBigEndian(this.H3, var1, 44);
		Pack.longToBigEndian(this.H4, var1, 52);
		Pack.longToBigEndian(this.H5, var1, 60);
		Pack.longToBigEndian(this.H6, var1, 68);
		Pack.longToBigEndian(this.H7, var1, 76);
		Pack.longToBigEndian(this.H8, var1, 84);
		Pack.intToBigEndian(this.wOff, var1, 92);

		while (var2 < this.wOff) {
			Pack.longToBigEndian(this.W[var2], var1, 96 + var2 * 8);
			++var2;
		}

	}

	protected void processBlock() {
		this.adjustByteCounts();

		for (int var1 = 16; var1 <= 79; ++var1) {
			this.W[var1] = this.Sigma1(this.W[var1 - 2]) + this.W[var1 - 7] + this.Sigma0(this.W[var1 - 15])
					+ this.W[var1 - 16];
		}

		long var2 = this.H1;
		long var4 = this.H2;
		long var6 = this.H3;
		long var8 = this.H4;
		long var10 = this.H5;
		long var12 = this.H6;
		long var14 = this.H7;
		long var16 = this.H8;
		int var18 = 0;
		long var19 = var16;
		long var21 = var2;

		int var95;
		for (int var23 = 0; var18 < 10; var23 = var95) {
			long var24 = this.Sum1(var10) + this.Ch(var10, var12, var14) + K[var23];
			long[] var26 = this.W;
			int var27 = var23 + 1;
			long var28 = var19 + var24 + var26[var23];
			long var30 = var8 + var28;
			long var32 = var28 + this.Sum0(var21) + this.Maj(var21, var4, var6);
			long var34 = this.Sum1(var30) + this.Ch(var30, var10, var12) + K[var27];
			long[] var36 = this.W;
			int var37 = var27 + 1;
			long var38 = var14 + var34 + var36[var27];
			long var40 = var6 + var38;
			long var42 = var38 + this.Sum0(var32) + this.Maj(var32, var21, var4);
			long var44 = this.Sum1(var40) + this.Ch(var40, var30, var10) + K[var37];
			long[] var46 = this.W;
			int var47 = var37 + 1;
			long var48 = var12 + var44 + var46[var37];
			long var50 = var4 + var48;
			long var52 = var48 + this.Sum0(var42) + this.Maj(var42, var32, var21);
			long var54 = this.Sum1(var50) + this.Ch(var50, var40, var30) + K[var47];
			long[] var56 = this.W;
			int var57 = var47 + 1;
			long var58 = var10 + var54 + var56[var47];
			long var60 = var21 + var58;
			long var62 = var58 + this.Sum0(var52) + this.Maj(var52, var42, var32);
			long var64 = this.Sum1(var60) + this.Ch(var60, var50, var40) + K[var57];
			long[] var66 = this.W;
			int var67 = var57 + 1;
			long var68 = var30 + var64 + var66[var57];
			long var70 = var32 + var68;
			var8 = var68 + this.Sum0(var62) + this.Maj(var62, var52, var42);
			long var72 = this.Sum1(var70) + this.Ch(var70, var60, var50) + K[var67];
			long[] var74 = this.W;
			int var75 = var67 + 1;
			long var76 = var40 + var72 + var74[var67];
			long var78 = var42 + var76;
			long var80 = var76 + this.Sum0(var8) + this.Maj(var8, var62, var52);
			long var82 = this.Sum1(var78) + this.Ch(var78, var70, var60) + K[var75];
			long[] var84 = this.W;
			int var85 = var75 + 1;
			long var86 = var50 + var82 + var84[var75];
			long var88 = var52 + var86;
			long var90 = var86 + this.Sum0(var80) + this.Maj(var80, var8, var62);
			long var92 = this.Sum1(var88) + this.Ch(var88, var78, var70) + K[var85];
			long[] var94 = this.W;
			var95 = var85 + 1;
			long var96 = var60 + var92 + var94[var85];
			var10 = var62 + var96;
			long var98 = var96 + this.Sum0(var90) + this.Maj(var90, var80, var8);
			++var18;
			var4 = var90;
			var6 = var80;
			var19 = var70;
			var21 = var98;
			var12 = var88;
			var14 = var78;
		}

		this.H1 += var21;
		this.H2 += var4;
		this.H3 += var6;
		this.H4 += var8;
		this.H5 += var10;
		this.H6 += var12;
		this.H7 += var14;
		this.H8 += var19;
		this.wOff = 0;

		for (int var100 = 0; var100 < 16; ++var100) {
			this.W[var100] = 0L;
		}

	}

	protected void processLength(long var1, long var3) {
		if (this.wOff > 14) {
			this.processBlock();
		}

		this.W[14] = var3;
		this.W[15] = var1;
	}

	protected void processWord(byte[] var1, int var2) {
		this.W[this.wOff] = Pack.bigEndianToLong(var1, var2);
		int var3 = 1 + this.wOff;
		this.wOff = var3;
		if (var3 == 16) {
			this.processBlock();
		}

	}

	public void reset() {
		int var1 = 0;
		this.byteCount1 = 0L;
		this.byteCount2 = 0L;
		this.xBufOff = 0;

		for (int var2 = 0; var2 < this.xBuf.length; ++var2) {
			this.xBuf[var2] = 0;
		}

		for (this.wOff = 0; var1 != this.W.length; ++var1) {
			this.W[var1] = 0L;
		}

	}

	protected void restoreState(byte[] var1) {
		int var2 = 0;
		this.xBufOff = Pack.bigEndianToInt(var1, 8);
		System.arraycopy(var1, 0, this.xBuf, 0, this.xBufOff);
		this.byteCount1 = Pack.bigEndianToLong(var1, 12);
		this.byteCount2 = Pack.bigEndianToLong(var1, 20);
		this.H1 = Pack.bigEndianToLong(var1, 28);
		this.H2 = Pack.bigEndianToLong(var1, 36);
		this.H3 = Pack.bigEndianToLong(var1, 44);
		this.H4 = Pack.bigEndianToLong(var1, 52);
		this.H5 = Pack.bigEndianToLong(var1, 60);
		this.H6 = Pack.bigEndianToLong(var1, 68);
		this.H7 = Pack.bigEndianToLong(var1, 76);
		this.H8 = Pack.bigEndianToLong(var1, 84);

		for (this.wOff = Pack.bigEndianToInt(var1, 92); var2 < this.wOff; ++var2) {
			this.W[var2] = Pack.bigEndianToLong(var1, 96 + var2 * 8);
		}

	}

	public void update(byte var1) {
		byte[] var2 = this.xBuf;
		int var3 = this.xBufOff++;
		var2[var3] = var1;
		if (this.xBufOff == this.xBuf.length) {
			this.processWord(this.xBuf, 0);
			this.xBufOff = 0;
		}

		++this.byteCount1;
	}

	public void update(byte[] var1, int var2, int var3) {
		while (this.xBufOff != 0 && var3 > 0) {
			this.update(var1[var2]);
			++var2;
			--var3;
		}

		while (var3 > this.xBuf.length) {
			this.processWord(var1, var2);
			var2 += this.xBuf.length;
			var3 -= this.xBuf.length;
			this.byteCount1 += (long) this.xBuf.length;
		}

		while (var3 > 0) {
			this.update(var1[var2]);
			++var2;
			--var3;
		}

	}
}
