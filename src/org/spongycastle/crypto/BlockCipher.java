package org.spongycastle.crypto;

public interface BlockCipher {
	String getAlgorithmName();

	int getBlockSize();

	void init(boolean var1, CipherParameters var2);

	int processBlock(byte[] var1, int var2, byte[] var3, int var4);

	void reset();
}
