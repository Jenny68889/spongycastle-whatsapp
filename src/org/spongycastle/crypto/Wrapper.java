package org.spongycastle.crypto;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

public interface Wrapper {
	String getAlgorithmName();

	void init(boolean var1, CipherParameters var2);

	byte[] unwrap(byte[] var1, int var2, int var3) throws InvalidCipherTextException, BadPaddingException, IllegalBlockSizeException;

	byte[] wrap(byte[] var1, int var2, int var3);
}
